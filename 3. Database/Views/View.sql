CREATE VIEW dbo.VW_EVENTOS AS
SELECT U.UserName as Usuario,
		E.Nombre as NombreEvento,
		CONVERT(VARCHAR, E.FechaInicio, 103) + ' ' + CONVERT(VARCHAR, E.HoraInicio, 8) 
		+ ' - ' +
		CONVERT(VARCHAR, E.FechaFin, 103) + ' ' + CONVERT(VARCHAR, E.HoraFin, 8) as FechaEvento, 
		TE.Nombre as TipoEvento
FROM dbo.Evento E
	INNER JOIN dbo.TipoEvento TE (NOLOCK) ON E.IdTipoEvento = TE.IdTipoEvento
	INNER JOIN dbo.AspNetUsers U (NOLOCK) ON E.IdUsuarioCreador = U.Id;