UPDATE [dbo].[Evento] 	
	SET Nombre = 'CITA DENTISTA',
		Descripcion = 'EXTRACCIÓN MUELA DEL JUICIO',
		FechaInicio = CONVERT(DATE, '2023-07-09'),
	    HoraInicio = CONVERT(TIME, '14:00:00'),
		FechaFin = CONVERT(DATE, '2023-07-09'),
		HoraFin = CONVERT(TIME, '16:00:00'),
		Local = 'CONSULTORIO DOCTOR 2',
		IdTipoEvento = 1 --EXCLUSIVO
WHERE IdEvento = 1;
