INSERT INTO [dbo].[Evento] 	
(
	[Nombre],
    [Descripcion],
    [FechaInicio],
    [HoraInicio],
    [FechaFin],
    [HoraFin],
    [Local],
    [IdTipoEvento],
    [IdUsuarioCreador],
    [FechaRegistro]	
)
VALUES
(
	'CITA DENTISTA',
	'CITA DENTISTA',
	CONVERT(DATE, '2023-07-09'),
	CONVERT(TIME, '09:00:00'),
	CONVERT(DATE, '2023-07-09'),
	CONVERT(TIME, '11:00:00'),
	'CONSULTORIO DOCTOR',
	1, --EXCLUSIVO
	'1ec0e447-6149-4889-a039-2420a1140b52', 
	GETDATE()	
);
