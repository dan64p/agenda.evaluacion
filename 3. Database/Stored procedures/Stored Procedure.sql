﻿CREATE PROCEDURE dbo.spGetEventosByFilter ( 
	@Filter nvarchar(300) = NULL,
	@IdUsuario nvarchar(450) = NULL,
	@Fecha DATETIME = NULL
)
AS
BEGIN
	SELECT *
	FROM dbo.Evento E
		INNER JOIN dbo.TipoEvento TE (NOLOCK) ON E.IdTipoEvento = TE.IdTipoEvento
		INNER JOIN dbo.AspNetUsers U (NOLOCK) ON E.IdUsuarioCreador = U.Id
	WHERE E.Nombre Like '%' + COALESCE(@Filter, E.Nombre)  + '%'
	AND E.IdUsuarioCreador = COALESCE(@IdUsuario, E.IdUsuarioCreador)
	AND (
		@Fecha IS NULL OR
		@Fecha BETWEEN E.FechaHoraInicio AND E.FechaHoraFin
	);
END
GO