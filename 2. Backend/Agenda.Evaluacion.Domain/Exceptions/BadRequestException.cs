﻿namespace Agenda.Evaluacion.Domain.Exceptions
{
    public abstract class BadRequestException : ApplicationException
    {
        protected BadRequestException(string message)
            : base("Parámetro Incorrectos", message)
        {
        }
    }
}