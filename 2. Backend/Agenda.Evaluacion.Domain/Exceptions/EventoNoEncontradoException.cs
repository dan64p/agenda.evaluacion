﻿namespace Agenda.Evaluacion.Domain.Exceptions
{
    public class EventoNoEncontradoException : NotFoundException
    {
        public EventoNoEncontradoException(string message = null)
            : base("No se encontró el evento buscado.")
        {
        }
    }
}
