﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Models.Filters
{
    public class AgendaEventoFiltro
    {
        public string IdUsuario { get; set; } = string.Empty;
        private DateTime? _fechaEvento;
        public DateTime? FechaEvento {
            get {
                return _fechaEvento;
            }
            set {
                if (value.HasValue)
                    _fechaEvento = new DateTime(value.Value.Year, value.Value.Month, value.Value.Day);
                else
                    _fechaEvento = null;
            }
        }

        private TimeSpan? _horaEvento;
        public TimeSpan? HoraEvento {
            get {
                return _horaEvento;
            }
            set { 
                if(value.HasValue)
                    _horaEvento = new TimeSpan(value.Value.Hours, value.Value.Minutes, value.Value.Seconds);
                else
                    _horaEvento = null;
            }
        }
        public string? FiltroEvento { get; set; } = string.Empty;
    }
}
