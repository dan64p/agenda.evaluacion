﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Models.Filters
{
    public class ExisteEventoExclusivoFiltro
    {
        public int IdEvento { get; set; }
        public string IdUsuarioCreador { get; set; } = string.Empty;
        public int IdTipoEvento { get; set; }
        public DateTime FechaInicio { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public TimeSpan HoraFin { get; set; }

        public DateTime FechaHoraInicio { 
            get { 
                return new DateTime(
                    FechaInicio.Year,
                    FechaInicio.Month,
                    FechaInicio.Day,
                    HoraInicio.Hours,
                    HoraInicio.Minutes,
                    HoraInicio.Seconds
                );
            } 
        }

        public DateTime FechaHoraFin {
            get { 
                return new DateTime(
                    FechaFin.Year,
                    FechaFin.Month,
                    FechaFin.Day,
                    HoraFin.Hours,
                    HoraFin.Minutes,
                    HoraFin.Seconds
                );
            }
        }
    }
}
