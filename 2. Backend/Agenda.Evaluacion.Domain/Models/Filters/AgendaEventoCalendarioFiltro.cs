﻿namespace Agenda.Evaluacion.Domain.Models.Filters
{
    public class AgendaEventoCalendarioFiltro
    {
        public string IdUsuario { get; set; } = string.Empty;
        public string? FiltroEvento { get; set; } = string.Empty;
        public int? Dia { get; set; }
        public int Mes { get; set; }
        public int Anio { get; set; }
        public TimeSpan? Hora { get; set; }
    }
}
