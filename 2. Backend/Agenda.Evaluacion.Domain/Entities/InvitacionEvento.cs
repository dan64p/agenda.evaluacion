﻿using Agenda.Evaluacion.Domain.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Entities
{
    public sealed class InvitacionEvento
    {
        public int IdInvitacionEvento { get; private set; }
        public int IdEvento { get; private set; }
        public Evento Evento { get; private set; }
        public bool FueAtendida { get; private set; }
        public bool FueAceptada { get; private set; }
        public string IdUsuarioInvitado { get; private set; }
        public Usuario UsuarioInvitado { get; private set; }
        public string IdUsuarioRegistro { get; set; }
        public Usuario UsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }

        public InvitacionEvento(
            int idEvento,
            string idUsuarioInvitado,
            string idUsuarioRegistro
        )
        {
            IdEvento = idEvento;
            IdUsuarioInvitado = idUsuarioInvitado;
            IdUsuarioRegistro = idUsuarioRegistro;
        }
        private InvitacionEvento()
        {
        }

        public void AceptarInvitacionEvento() {
            FueAtendida = true;
            FueAceptada = true;
        }

        public void RechazarInvitacionEvento() {
            FueAtendida = true;
            FueAceptada = false;
        }
    }
}
