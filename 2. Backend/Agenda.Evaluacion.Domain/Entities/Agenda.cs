﻿using Agenda.Evaluacion.Domain.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Entities
{
    public sealed class Agenda
    {
        public int IdAgenda { get; private set; }
        public string Nombre { get; private set; } = string.Empty;
        public string Descripcion { get; set; } = string.Empty;
        public string IdUsuario { get; private set; }
        public Usuario Usuario { get; private set; }

        public Agenda(
            string nombre,
            string descripcion,
            string idUsuario
        )
            : this()
        {
            Nombre = nombre;
            Descripcion = descripcion;
            IdUsuario = idUsuario;
        }

        public Agenda(
            int idAgenda,
            string nombre,
            string descripcion,
            string idUsuario
        )
            : this()
        {
            IdAgenda = idAgenda;
            Nombre = nombre;
            Descripcion = descripcion;
            IdUsuario = idUsuario;
        }

        private Agenda()
        {
        }
    }
}
