﻿using Agenda.Evaluacion.Domain.Entities.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Entities
{
    public sealed class Evento
    {
        public int IdEvento { get; private set; }
        public string Nombre { get; private set; } = string.Empty;
        public string Descripcion { get; private set; } = string.Empty;
        
        [DataType(DataType.Date)]
        public DateTime FechaInicio { get; private set; }
        
        public TimeSpan HoraInicio { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [DataType(DataType.DateTime)]
        public DateTime FechaHoraInicio { get; set; }

        [DataType(DataType.Date)]
        public DateTime FechaFin { get; private set; }

        public TimeSpan HoraFin { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [DataType(DataType.DateTime)]
        public DateTime FechaHoraFin { get; set; }

        public string Local { get; private set; } = string.Empty;
        public int IdTipoEvento { get; private set; }
        public TipoEvento TipoEvento { get; private set; }
        public ICollection<ParticipanteEvento>? Participantes { get; private set; }
        public string IdUsuarioCreador { get; private set; }
        public Usuario UsuarioCreador { get; private set; }
        public DateTime FechaRegistro { get; private set; }

        public Evento(
            string nombre,
            string descripcion,
            DateTime fechaInicio,
            TimeSpan horaInicio,
            DateTime fechaFin,
            TimeSpan horaFin,
            string local,
            int idTipoEvento,
            ICollection<ParticipanteEvento>? participantes,
            string idUsuarioCreador
        )
        {
            Nombre = nombre;
            Descripcion = descripcion;
            EditarCamposFechaHora(fechaInicio, horaInicio, fechaFin, horaFin);
            Local = local;
            IdTipoEvento = idTipoEvento;
            Participantes = participantes;
            IdUsuarioCreador = idUsuarioCreador;
            FechaRegistro = DateTime.Now;
        }

        private Evento()
        {
        }

        public void ActualizarEvento(
            string nombre,
            string descripcion,
            DateTime fechaInicio,
            TimeSpan horaInicio,
            DateTime fechaFin,
            TimeSpan horaFin,
            string local,
            int idTipoEvento,
            ICollection<ParticipanteEvento>? participantes
        ) {

            Nombre = nombre;
            Descripcion = descripcion;
            EditarCamposFechaHora(fechaInicio, horaInicio, fechaFin, horaFin);
            Local = local;
            IdTipoEvento = idTipoEvento;

            if (Participantes != null)
            {
                foreach (var participante in Participantes)
                    Participantes.Remove(participante);

                if (participantes != null)
                    foreach (var participanteNuevo in participantes)
                        Participantes.Add(participanteNuevo);
            }
            else
            {
                Participantes = participantes;
            }
        }

        private void EditarCamposFechaHora(
            DateTime fechaInicio,
            TimeSpan horaInicio,
            DateTime fechaFin,
            TimeSpan horaFin
        )
        {
            FechaInicio = new DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day);
            HoraInicio = new TimeSpan(horaInicio.Hours, horaInicio.Minutes, 0);
            FechaFin = new DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day);
            HoraFin = new TimeSpan(horaFin.Hours, horaFin.Minutes, 0);
        }
    }

    public sealed class ParticipanteEvento
    {
        public int IdParticipanteEvento { get; private set; }
        public string NombreCompleto { get; private set; } = string.Empty;
        public int? IdUsuario { get; private set; }
        public Usuario? Usuario { get; private set; }
        public int IdEvento { get; set; }

        public ParticipanteEvento(
            string nombreCompleto,
            int? idUsuario = null
        )
        {
            NombreCompleto = nombreCompleto;
            IdUsuario = idUsuario;
        }

        private ParticipanteEvento()
        {
            
        }
    }
}
