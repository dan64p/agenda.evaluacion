﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Entities
{
    public sealed class TipoEvento
    {
        public int IdTipoEvento { get; private set; }
        public string Nombre { get; private set; } = string.Empty;
        public bool EsCompartible { get; private set; }

        public TipoEvento(
             string nombre,
             bool esCompartible
        )
        {
            Nombre = nombre;
            EsCompartible = esCompartible;
        }

        public TipoEvento(
             int idTipoEvento,
             string nombre,
             bool esCompartible
        )
        {
            IdTipoEvento = idTipoEvento;
            Nombre = nombre;
            EsCompartible = esCompartible;
        }

        private TipoEvento()
        {
            
        }
    }
}
