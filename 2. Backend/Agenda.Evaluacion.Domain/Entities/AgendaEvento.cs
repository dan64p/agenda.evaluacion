﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Entities
{
    public sealed class AgendaEvento
    {
        public int IdAgendaEvento { get; private set; }
        public int IdAgenda { get; private set; }
        public Agenda Agenda { get; private set; }
        public int IdEvento { get; private set; }
        public Evento Evento { get; private set; }
        public int? IdInvitacionEvento { get; private set; }
        public InvitacionEvento? InvitacionEvento { get; private set; }

        public AgendaEvento(
            int idAgenda,
            int idEvento,
            int? idInvitacionEvento = null
        )
        {
            IdAgenda = idAgenda;
            IdEvento = idEvento;
            IdInvitacionEvento = idInvitacionEvento;
        }

        private AgendaEvento()
        {
        }
    }
}
