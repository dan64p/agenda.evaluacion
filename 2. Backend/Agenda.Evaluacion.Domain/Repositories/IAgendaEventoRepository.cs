﻿using Agenda.Evaluacion.Domain.Entities;
using Agenda.Evaluacion.Domain.Models.Filters;
using Agenda.Evaluacion.Domain.Repositories.Base;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Repositories
{
    public interface IAgendaEventoRepository : ISearchableRepository<AgendaEvento, AgendaEventoFiltro>
    {
        Task DeleteByEvento(Evento evento);
        Task<IEnumerable<AgendaEvento>> SearchCalendarioByFilter(AgendaEventoCalendarioFiltro filter, CancellationToken cancellationToken = default);
    }
}
