﻿using System.Threading;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        void EnableMultipleChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        void SaveAllChanges();
        void UndoAllChanges();
    }
}
