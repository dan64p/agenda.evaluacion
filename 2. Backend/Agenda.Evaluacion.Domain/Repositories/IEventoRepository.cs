﻿using Agenda.Evaluacion.Domain.Entities;
using Agenda.Evaluacion.Domain.Models.Filters;
using Agenda.Evaluacion.Domain.Repositories.Base;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Repositories
{
    public interface IEventoRepository : IRepository<Evento>
    {
        Task<bool> ExisteEventoExclusivoEntreFechas(ExisteEventoExclusivoFiltro filtro, CancellationToken cancellationToken);
    }
}
