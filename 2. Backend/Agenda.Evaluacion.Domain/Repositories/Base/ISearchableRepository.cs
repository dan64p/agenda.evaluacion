﻿using Agenda.Evaluacion.Common.Communication.RequestUtils;
using Agenda.Evaluacion.Common.Communication.ResponseUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Domain.Repositories.Base
{
    public interface ISearchableRepository<T, REQF> : IRepository<T>
    {
        Task<IEnumerable<T>> SearchByFilter(REQF filter, CancellationToken cancellationToken = default);
        Task<PaginatedResponse<T>> SearchPaginatedByFilter(PaginatedRequest<REQF> filter, CancellationToken cancellationToken = default);
    }
}
