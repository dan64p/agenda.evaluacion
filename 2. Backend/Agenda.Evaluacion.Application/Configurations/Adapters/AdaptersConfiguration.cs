﻿using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Domain.Entities;
using EventoEntidad = Agenda.Evaluacion.Domain.Entities.Evento;
using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agenda.Evaluacion.Common;
using static Agenda.Evaluacion.Application.Models.Evento.EventoViewModel;
using Agenda.Evaluacion.Application.Evento.Commands.CrearEvento;
using Agenda.Evaluacion.Application.Evento.Commands.ActualizarEvento;
using Agenda.Evaluacion.Common.Communication.ResponseUtils;
using Agenda.Evaluacion.Domain.Models.Filters;

namespace Agenda.Evaluacion.Application.Configurations.Adapters
{
    public static class AdaptersConfiguration
    {
        public static void Configure() {
            TypeAdapterConfig<ActualizarEventoCommand, ExisteEventoExclusivoFiltro>
                .NewConfig()
                .Map(dest => dest.IdUsuarioCreador, orig => orig.IdUsuarioActualiza);

            TypeAdapterConfig<AgendaEvento, EventoGridViewModel>
                .NewConfig()
                .Map(dest => dest.IdAgendaEvento, orig => orig.IdAgendaEvento)
                .Map(dest => dest.IdEvento, orig => orig.IdEvento)
                .Map(dest => dest.IdAgenda, orig => orig.IdAgenda)
                .Map(dest => dest.NombreEvento, orig => orig.Evento.Nombre)
                .Map(dest => dest.DescripcionEvento, orig => orig.Evento.Descripcion)
                .Map(dest => dest.FechaHoraEvento, orig =>
                    orig.Evento.FechaInicio.ToString(Constants.DateFormat_Latino) + " "
                    + orig.Evento.HoraInicio.ToString(Constants.TimeFormat_Latino)
                    + " - "
                    + orig.Evento.FechaFin.ToString(Constants.DateFormat_Latino) + " "
                    + orig.Evento.HoraFin.ToString(Constants.TimeFormat_Latino)
                )
                .Map(dest => dest.IdUsuarioCreador, orig => orig.Evento.IdUsuarioCreador)
                .Map(dest => dest.NombreUsuarioCreador, orig => orig.Evento.UsuarioCreador.NombreCompleto)
                .Map(dest => dest.FechaRegistro, orig => orig.Evento.FechaRegistro.ToString(Constants.DateTimeFormat_Latino))
                .Map(dest => dest.IdTipoEvento, orig => orig.Evento.IdTipoEvento)
                .Map(dest => dest.NombreTipoEvento, orig => orig.Evento.TipoEvento.Nombre);

            TypeAdapterConfig<AgendaEvento, EventoCalendarioViewModel>
                .NewConfig()
                .Map(dest => dest.IdAgendaEvento, orig => orig.IdAgendaEvento)
                .Map(dest => dest.IdEvento, orig => orig.IdEvento)
                .Map(dest => dest.IdAgenda, orig => orig.IdAgenda)
                .Map(dest => dest.NombreEvento, orig => orig.Evento.Nombre)
                .Map(dest => dest.FechaHoraInicio, orig => orig.Evento.FechaHoraInicio)
                .Map(dest => dest.FechaHoraFin, orig => orig.Evento.FechaHoraFin);

            TypeAdapterConfig<PaginatedResponse<AgendaEvento>, PaginatedResponse<EventoGridViewModel>>
                .NewConfig()
                .Map(dest => dest.Data, orig => orig.Data.Adapt<IEnumerable<EventoGridViewModel>>());

            TypeAdapterConfig<EventoEntidad, EventoViewModel>
                .NewConfig()
                .Map(dest => dest.IdEvento, orig => orig.IdEvento)
                .Map(dest => dest.Nombre, orig => orig.Nombre)
                .Map(dest => dest.Descripcion, orig => orig.Descripcion)
                .Map(dest => dest.FechaInicio, orig => orig.FechaInicio)
                .Map(dest => dest.HoraInicio, orig => new DateTime(orig.HoraInicio.Ticks))
                .Map(dest => dest.FechaFin, orig => orig.FechaFin)
                .Map(dest => dest.HoraFin, orig => new DateTime(orig.HoraFin.Ticks))
                .Map(dest => dest.IdTipoEvento, orig => orig.IdTipoEvento)
                .Map(
                    dest => dest.Participantes, 
                    orig => orig.Participantes.Adapt<IEnumerable<ParticipanteEventoViewModel>>()
                 );

            TypeAdapterConfig<ParticipanteCrearEventoCommand, ParticipanteEvento>
                .NewConfig()
                .MapToConstructor(true);

            TypeAdapterConfig<ParticipanteActualizarEventoCommand, ParticipanteEvento>
                .NewConfig()
                .MapToConstructor(true);
        }
    }
}
