﻿using MediatR;

namespace Agenda.Evaluacion.Application.Abstractions.Messaging
{
    public interface ICommand<out TResponse> : IRequest<TResponse>
    {
    }
}