﻿using MediatR;

namespace Agenda.Evaluacion.Application.Abstractions.Messaging
{
    public interface IQuery<out TResponse> : IRequest<TResponse>
    {
    }
}