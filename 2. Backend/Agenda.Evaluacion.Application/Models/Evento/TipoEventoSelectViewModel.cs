﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Application.Models.Evento
{
    public class TipoEventoSelectViewModel
    {
        public int IdTipoEvento { get; set; }
        public string Nombre { get; set; } = string.Empty;
        public bool EsCompartible { get; set; }
    }
}
