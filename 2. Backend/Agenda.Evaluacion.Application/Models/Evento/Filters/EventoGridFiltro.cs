﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Application.Models.Evento.Filters
{
    public class EventoGridFiltro
    {
        public string IdUsuario { get; set; } = string.Empty;
        public DateTime? FechaEvento { get; set; }
        public TimeSpan? HoraEvento { get; set; }
        public string? FiltroEvento { get; set; } = string.Empty;
    }
}
