﻿namespace Agenda.Evaluacion.Application.Models.Evento.Filters
{
    public class EventoCalendarioFiltro
    {
        public string IdUsuario { get; set; } = string.Empty;
        public string? FiltroEvento { get; set; } = string.Empty;
        public int? Dia { get; set; }
        public int Mes { get; set; }
        public int Anio { get; set; }
        public TimeSpan? Hora { get; set; }
    }
}
