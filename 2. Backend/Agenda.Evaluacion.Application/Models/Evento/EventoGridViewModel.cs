﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Application.Models.Evento
{
    public class EventoGridViewModel
    {
        public int IdAgendaEvento { get; set; }
        public int IdAgenda { get; set; }
        public int IdEvento { get; set; }
        public string NombreEvento { get; set; } = string.Empty;
        public string DescripcionEvento { get; set; } = string.Empty;
        public string FechaHoraEvento { get; set; } = string.Empty;
        public string IdUsuarioCreador { get; set; } = string.Empty;
        public string NombreUsuarioCreador { get; set; } = string.Empty;
        public string FechaRegistro { get; set; } = string.Empty;
        public int IdTipoEvento { get; set; }
        public string NombreTipoEvento { get; set; } = string.Empty;
    }
}
