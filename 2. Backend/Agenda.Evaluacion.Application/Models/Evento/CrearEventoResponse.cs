﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Application.Models.Evento
{
    public class CrearEventoResponse
    {
        public int IdEvento { get; set; }
        public string Nombre { get; set; } = string.Empty;
    }
}
