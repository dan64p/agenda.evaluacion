﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Application.Models.Evento
{
    public class EventoViewModel
    {
        public int IdEvento { get; set; }
        public string Nombre { get; set; } = string.Empty;
        public string Descripcion { get; set; } = string.Empty;
        public DateTime FechaInicio { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public DateTime HoraFin { get; set; }
        public string Local { get; set; } = string.Empty;
        public int IdTipoEvento { get; set; }

        public IEnumerable<ParticipanteEventoViewModel> Participantes { get; set; } = new List<ParticipanteEventoViewModel>();

        public class ParticipanteEventoViewModel
        {
            public string NombreCompleto { get; set; } = string.Empty;
        }
    }
}
