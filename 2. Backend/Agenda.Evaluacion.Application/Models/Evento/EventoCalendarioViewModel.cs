﻿namespace Agenda.Evaluacion.Application.Models.Evento
{
    public class EventoCalendarioViewModel
    {
        public int IdAgendaEvento { get; set; }
        public int IdAgenda { get; set; }
        public int IdEvento { get; set; }
        public string NombreEvento { get; set; } = string.Empty;
        public DateTime FechaHoraInicio { get; set; }
        public DateTime FechaHoraFin { get; set; }
    }
}
