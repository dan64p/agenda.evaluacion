﻿namespace Agenda.Evaluacion.Application.Evento.Commands.ActualizarEvento
{
    public sealed record ActualizarEventoRequest(
        int IdEvento,
        string Nombre,
        string Descripcion,
        DateTime FechaInicio,
        TimeSpan HoraInicio,
        DateTime FechaFin,
        TimeSpan HoraFin,
        string Local,
        int IdTipoEvento,
        IEnumerable<ParticipanteActualizarEventoRequest>? Participantes,
        string IdUsuarioActualiza
    );

    public sealed record ParticipanteActualizarEventoRequest(
        string NombreCompleto,
        int? IdUsuario
    );
}
