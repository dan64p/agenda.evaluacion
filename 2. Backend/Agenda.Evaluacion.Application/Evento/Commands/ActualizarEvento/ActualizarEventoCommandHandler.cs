﻿using System.Threading;
using System.Threading.Tasks;
using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Domain.Repositories;
using EventoEntidad = Agenda.Evaluacion.Domain.Entities.Evento;
using Mapster;
using Agenda.Evaluacion.Domain.Entities;
using Agenda.Evaluacion.Application.Evento.Commands.CrearEvento;
using Agenda.Evaluacion.Domain.Exceptions;

namespace Agenda.Evaluacion.Application.Evento.Commands.ActualizarEvento
{
    internal sealed class ActualizarEventoCommandHandler : ICommandHandler<ActualizarEventoCommand, ActualizarEventoResponse>
    {
        private readonly IEventoRepository _eventoRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ActualizarEventoCommandHandler(
            IEventoRepository eventoRepository, 
            IUnitOfWork unitOfWork
        )
        {
            _eventoRepository = eventoRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ActualizarEventoResponse> Handle(ActualizarEventoCommand request, CancellationToken cancellationToken)
        {
            var evento = await _eventoRepository.GetByIdAsync(request.IdEvento, cancellationToken);

            if (evento == null)
                throw new EventoNoEncontradoException();

            evento.ActualizarEvento(
                request.Nombre,
                request.Descripcion,
                request.FechaInicio,
                request.HoraInicio,
                request.FechaFin,
                request.HoraFin,
                request.Local,
                request.IdTipoEvento,
                request.Participantes != null ? 
                    request.Participantes.Select(p => new ParticipanteEvento(p.NombreCompleto, p.IdUsuario)).ToList() : null
            );

            _eventoRepository.Update(evento);

            await _unitOfWork.SaveChangesAsync(cancellationToken);

            return evento.Adapt<ActualizarEventoResponse>();
        }
    }
}