﻿using Agenda.Evaluacion.Common.Catalogs;
using Agenda.Evaluacion.Domain.Models.Filters;
using Agenda.Evaluacion.Domain.Repositories;
using FluentValidation;
using Mapster;

namespace Agenda.Evaluacion.Application.Evento.Commands.ActualizarEvento
{
    public sealed class ActualizarEventoCommandValidator : AbstractValidator<ActualizarEventoCommand>
    {
        public ActualizarEventoCommandValidator(IEventoRepository eventoRepository)
        {
            RuleFor(x => x.IdEvento).NotEmpty();

            RuleFor(x => x.Nombre).NotEmpty().MaximumLength(200);

            RuleFor(x => x.Descripcion).NotEmpty().MaximumLength(500);

            RuleFor(x => x.FechaInicio).NotEmpty();

            RuleFor(x => x.HoraInicio).NotEmpty();

            RuleFor(x => x.FechaFin).NotEmpty();

            RuleFor(x => x.HoraFin).NotEmpty();

            RuleFor(x => x.IdTipoEvento).NotEmpty();

            RuleFor(x => x.IdUsuarioActualiza).NotEmpty();

            RuleFor(x => x).MustAsync(async (command, cancellation) =>
            {
                if (command.IdTipoEvento == (int) TipoEventoEnum.Compartido)
                    return true;

                var existeEventoExclusivo = await eventoRepository
                    .ExisteEventoExclusivoEntreFechas(
                        command.Adapt<ExisteEventoExclusivoFiltro>(),
                        cancellation
                );

                return !existeEventoExclusivo;
            }).WithMessage("Ya existe otro evento exlusivo registrado entre las fechas inicio y fin ingresadas.");
        }
    }
}
