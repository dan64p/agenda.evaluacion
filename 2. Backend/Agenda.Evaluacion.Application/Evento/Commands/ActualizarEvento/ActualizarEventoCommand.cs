﻿
using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;

namespace Agenda.Evaluacion.Application.Evento.Commands.ActualizarEvento
{
    public sealed record ActualizarEventoCommand(
            int IdEvento,
            string Nombre, 
            string Descripcion,
            DateTime FechaInicio,
            TimeSpan HoraInicio,
            DateTime FechaFin,
            TimeSpan HoraFin,
            string Local,
            int IdTipoEvento,
            IEnumerable<ParticipanteActualizarEventoCommand>? Participantes,
            string IdUsuarioActualiza

     ) : ICommand<ActualizarEventoResponse>;

    public sealed record ParticipanteActualizarEventoCommand(
        string NombreCompleto,
        int? IdUsuario
    );
}
