﻿using Agenda.Evaluacion.Domain.Repositories;
using FluentValidation;

namespace Agenda.Evaluacion.Application.Evento.Commands.EliminarEvento
{
    public sealed class EliminarEventoCommandValidator : AbstractValidator<EliminarEventoCommand>
    {
        public EliminarEventoCommandValidator(IEventoRepository eventoRepository)
        {
            RuleFor(x => x.IdEvento).NotEmpty();
            RuleFor(x => x.IdUsuarioElimina).NotEmpty();

            RuleFor(x => x).MustAsync(async (command, cancellation) =>
            {
                var evento = await eventoRepository.GetByIdAsync(command.IdEvento, cancellation);

                if (evento == null)
                    return true;

                return evento.IdUsuarioCreador == command.IdUsuarioElimina;
            }).WithMessage("No se puede eliminar el evento de otro usuario.");
        }
    }
}