﻿using System.Threading;
using System.Threading.Tasks;
using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Domain.Repositories;
using EventoEntidad = Agenda.Evaluacion.Domain.Entities.Evento;
using Mapster;
using Agenda.Evaluacion.Domain.Entities;
using Agenda.Evaluacion.Domain.Exceptions;

namespace Agenda.Evaluacion.Application.Evento.Commands.EliminarEvento
{
    internal sealed class EliminarEventoCommandHandler : ICommandHandler<EliminarEventoCommand, EliminarEventoResponse>
    {
        private readonly IEventoRepository _eventoRepository;
        private readonly IAgendaEventoRepository _agendaEventoRepository;
        private readonly IUnitOfWork _unitOfWork;

        public EliminarEventoCommandHandler(
            IEventoRepository eventoRepository, 
            IAgendaEventoRepository agendaEventoRepository,
            IUnitOfWork unitOfWork
        )
        {
            _eventoRepository = eventoRepository;
            _agendaEventoRepository = agendaEventoRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<EliminarEventoResponse> Handle(EliminarEventoCommand request, CancellationToken cancellationToken)
        {
            _unitOfWork.EnableMultipleChanges();

            var evento = await _eventoRepository.GetByIdAsync(request.IdEvento, cancellationToken);

            if (evento == null)
                throw new EventoNoEncontradoException();

            await _agendaEventoRepository.DeleteByEvento(evento);

            await _unitOfWork.SaveChangesAsync(cancellationToken);

            _eventoRepository.Delete(evento);

            await _unitOfWork.SaveChangesAsync(cancellationToken);
            
            _unitOfWork.SaveAllChanges();

            return evento.Adapt<EliminarEventoResponse>();
        }
    }
}