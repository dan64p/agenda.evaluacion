﻿namespace Agenda.Evaluacion.Application.Evento.Commands.EliminarEvento
{
    public sealed record EliminarEventoRequest(
        int IdEvento,
        string IdUsuarioElimina
    );
}
