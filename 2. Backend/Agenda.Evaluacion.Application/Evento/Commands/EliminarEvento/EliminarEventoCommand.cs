﻿
using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;

namespace Agenda.Evaluacion.Application.Evento.Commands.EliminarEvento
{
    public sealed record EliminarEventoCommand(
        int IdEvento,
        string IdUsuarioElimina

     ) : ICommand<EliminarEventoResponse>;
}
