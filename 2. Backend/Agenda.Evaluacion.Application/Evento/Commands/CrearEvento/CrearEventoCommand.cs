﻿
using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;

namespace Agenda.Evaluacion.Application.Evento.Commands.CrearEvento
{
    public sealed record CrearEventoCommand(
            string Nombre, 
            string Descripcion,
            DateTime FechaInicio,
            TimeSpan HoraInicio,
            DateTime FechaFin,
            TimeSpan HoraFin,
            string Local,
            int IdTipoEvento,
            IEnumerable<ParticipanteCrearEventoCommand>? Participantes,
            string IdUsuarioCreador,
            int IdAgenda

     ) : ICommand<CrearEventoResponse>;

    public sealed record ParticipanteCrearEventoCommand(
        string NombreCompleto,
        int? IdUsuario
    );
}
