﻿namespace Agenda.Evaluacion.Application.Evento.Commands.CrearEvento
{
    public sealed record CrearEventoRequest(
        string Nombre,
        string Descripcion,
        DateTime FechaInicio,
        TimeSpan HoraInicio,
        DateTime FechaFin,
        TimeSpan HoraFin,
        string Local,
        int IdTipoEvento,
        IEnumerable<ParticipanteCrearEventoRequest>? Participantes,
        string IdUsuarioCreador,
        int IdAgenda
    );

    public sealed record ParticipanteCrearEventoRequest(
        string NombreCompleto,
        int? IdUsuario
    );
}
