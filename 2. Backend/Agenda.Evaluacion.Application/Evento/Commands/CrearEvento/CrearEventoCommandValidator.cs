﻿using Agenda.Evaluacion.Common.Catalogs;
using Agenda.Evaluacion.Domain.Models.Filters;
using Agenda.Evaluacion.Domain.Repositories;
using FluentValidation;
using Mapster;

namespace Agenda.Evaluacion.Application.Evento.Commands.CrearEvento
{
    public sealed class CrearEventoCommandValidator : AbstractValidator<CrearEventoCommand>
    {
        public CrearEventoCommandValidator(IEventoRepository eventoRepository)
        {
            RuleFor(x => x.Nombre).NotEmpty().MaximumLength(200);

            RuleFor(x => x.Descripcion).NotEmpty().MaximumLength(500);

            RuleFor(x => x.FechaInicio).NotEmpty();

            RuleFor(x => x.HoraInicio).NotEmpty();

            RuleFor(x => x.FechaFin).NotEmpty();

            RuleFor(x => x.HoraFin).NotEmpty();

            RuleFor(x => x.IdAgenda).NotEmpty();

            RuleFor(x => x.IdTipoEvento).NotEmpty();

            RuleFor(x => x.IdUsuarioCreador).NotEmpty();

            RuleFor(x => x).MustAsync(async (command, cancellation) =>
            {
                if (command.IdTipoEvento == (int)TipoEventoEnum.Compartido)
                    return true;

                return !(await eventoRepository
                    .ExisteEventoExclusivoEntreFechas(
                        command.Adapt<ExisteEventoExclusivoFiltro>(), 
                        cancellation
                ));
            }).WithMessage("Ya existe otro evento exlusivo registrado entre las fechas inicio y fin ingresadas.");
        }
    }
}