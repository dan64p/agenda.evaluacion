﻿using System.Threading;
using System.Threading.Tasks;
using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Domain.Repositories;
using EventoEntidad = Agenda.Evaluacion.Domain.Entities.Evento;
using Mapster;
using Agenda.Evaluacion.Domain.Entities;

namespace Agenda.Evaluacion.Application.Evento.Commands.CrearEvento
{
    internal sealed class CrearEventoCommandHandler : ICommandHandler<CrearEventoCommand, CrearEventoResponse>
    {
        private readonly IEventoRepository _eventoRepository;
        private readonly IAgendaEventoRepository _agendaEventoRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CrearEventoCommandHandler(
            IEventoRepository eventoRepository, 
            IAgendaEventoRepository agendaEventoRepository,
            IUnitOfWork unitOfWork
        )
        {
            _eventoRepository = eventoRepository;
            _agendaEventoRepository = agendaEventoRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<CrearEventoResponse> Handle(CrearEventoCommand request, CancellationToken cancellationToken)
        {
            _unitOfWork.EnableMultipleChanges();

            var evento = new EventoEntidad(
                request.Nombre,
                request.Descripcion,
                request.FechaInicio,
                request.HoraInicio,
                request.FechaFin,
                request.HoraFin,
                request.Local,
                request.IdTipoEvento,
                request.Participantes != null ? 
                    request.Participantes.Select(p => new ParticipanteEvento(p.NombreCompleto, p.IdUsuario)).ToList() : null,
                request.IdUsuarioCreador
            );

            _eventoRepository.Insert(evento);

            await _unitOfWork.SaveChangesAsync(cancellationToken);

            var agendaEvento = new AgendaEvento(request.IdAgenda, evento.IdEvento);

            _agendaEventoRepository.Insert(agendaEvento);

            await _unitOfWork.SaveChangesAsync();

            _unitOfWork.SaveAllChanges();

            return evento.Adapt<CrearEventoResponse>();
        }
    }
}