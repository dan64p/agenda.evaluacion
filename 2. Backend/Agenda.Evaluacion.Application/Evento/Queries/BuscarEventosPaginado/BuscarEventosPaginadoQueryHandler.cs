﻿using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Common.Communication.RequestUtils;
using Agenda.Evaluacion.Common.Communication.ResponseUtils;
using Agenda.Evaluacion.Domain.Models.Filters;
using Agenda.Evaluacion.Domain.Repositories;
using Mapster;

namespace Agenda.Evaluacion.Application.Evento.Queries.BuscarEventosPaginado
{
    internal sealed class BuscarEventosPaginadoQueryHandler : IQueryHandler<BuscarEventosPaginadoQuery, PaginatedResponse<EventoGridViewModel>>
    {
        private readonly IAgendaEventoRepository _agendaEventoRepository;

        public BuscarEventosPaginadoQueryHandler(IAgendaEventoRepository agendaEventoRepository) => _agendaEventoRepository = agendaEventoRepository;

        public async Task<PaginatedResponse<EventoGridViewModel>> Handle(
            BuscarEventosPaginadoQuery request, 
            CancellationToken cancellationToken
        )
        {
            var domainFilter = request.Filtro.Adapt<PaginatedRequest<AgendaEventoFiltro>>();
            domainFilter.Filter = domainFilter.Filter ?? new AgendaEventoFiltro();
            var paginatedResponse = await _agendaEventoRepository.SearchPaginatedByFilter(domainFilter, cancellationToken);

            var responseFormated = paginatedResponse.Adapt<PaginatedResponse<EventoGridViewModel>>();

            return responseFormated;
        }
    }
}