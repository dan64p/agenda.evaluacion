﻿using System.Collections.Generic;
using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Application.Models.Evento.Filters;
using Agenda.Evaluacion.Common.Communication.RequestUtils;
using Agenda.Evaluacion.Common.Communication.ResponseUtils;

namespace Agenda.Evaluacion.Application.Evento.Queries.BuscarEventosPaginado
{
    public sealed record BuscarEventosPaginadoQuery(PaginatedRequest<EventoGridFiltro> Filtro) : IQuery<PaginatedResponse<EventoGridViewModel>>;
}
