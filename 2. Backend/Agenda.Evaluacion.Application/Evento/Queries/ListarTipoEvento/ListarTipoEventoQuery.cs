﻿using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;

namespace Agenda.Evaluacion.Application.Evento.Queries.ListarTipoEvento
{
    public sealed record ListarTipoEventoQuery() : IQuery<IEnumerable<TipoEventoSelectViewModel>>;
}
