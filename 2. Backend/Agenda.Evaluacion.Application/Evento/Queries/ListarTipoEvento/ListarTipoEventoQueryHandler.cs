﻿using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Common.Communication.RequestUtils;
using Agenda.Evaluacion.Common.Communication.ResponseUtils;
using Agenda.Evaluacion.Domain.Models.Filters;
using Agenda.Evaluacion.Domain.Repositories;
using Mapster;

namespace Agenda.Evaluacion.Application.Evento.Queries.ListarTipoEvento
{
    internal sealed class ListarTipoEventoQueryHandler : IQueryHandler<ListarTipoEventoQuery, IEnumerable<TipoEventoSelectViewModel>>
    {
        private readonly ITipoEventoRepository _tipoEventoRepository;

        public ListarTipoEventoQueryHandler(ITipoEventoRepository tipoEventoRepository) => _tipoEventoRepository = tipoEventoRepository;

        public async Task<IEnumerable<TipoEventoSelectViewModel>> Handle(
            ListarTipoEventoQuery request, 
            CancellationToken cancellationToken
        )
        {
            var tiposEvento = await _tipoEventoRepository.GetAllAsync(cancellationToken ); ;

            return tiposEvento.Adapt<IEnumerable<TipoEventoSelectViewModel>>();
        }
    }
}