﻿using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Application.Models.Evento.Filters;
using Agenda.Evaluacion.Common.Communication.RequestUtils;
using Agenda.Evaluacion.Common.Communication.ResponseUtils;
using Agenda.Evaluacion.Domain.Models.Filters;
using Agenda.Evaluacion.Domain.Repositories;
using Mapster;

namespace Agenda.Evaluacion.Application.Evento.Queries.BuscarEventosCalendario
{
    internal sealed class BuscarEventosCalendarioQueryHandler : IQueryHandler<BuscarEventosCalendarioQuery, IEnumerable<EventoCalendarioViewModel>>
    {
        private readonly IAgendaEventoRepository _agendaEventoRepository;

        public BuscarEventosCalendarioQueryHandler(IAgendaEventoRepository agendaEventoRepository) => _agendaEventoRepository = agendaEventoRepository;

        public async Task<IEnumerable<EventoCalendarioViewModel>> Handle(
            BuscarEventosCalendarioQuery request, 
            CancellationToken cancellationToken
        )
        {
            var domainFilter = request.Filtro.Adapt<AgendaEventoCalendarioFiltro>();
            var eventos = await _agendaEventoRepository.SearchCalendarioByFilter(domainFilter, cancellationToken);

            var responseFormated = eventos.Adapt<IEnumerable<EventoCalendarioViewModel>>();

            return responseFormated;
        }
    }
}