﻿using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;

namespace Agenda.Evaluacion.Application.Evento.Queries.BuscarEventoPorId
{
    public sealed record BuscarEventoPorIdQuery(int IdEvento) : IQuery<EventoViewModel>;
}
