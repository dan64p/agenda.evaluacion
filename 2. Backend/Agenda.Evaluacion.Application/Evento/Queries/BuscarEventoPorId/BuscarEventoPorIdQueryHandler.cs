﻿using Agenda.Evaluacion.Application.Abstractions.Messaging;
using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Common.Communication.RequestUtils;
using Agenda.Evaluacion.Common.Communication.ResponseUtils;
using Agenda.Evaluacion.Domain.Exceptions;
using Agenda.Evaluacion.Domain.Models.Filters;
using Agenda.Evaluacion.Domain.Repositories;
using Mapster;

namespace Agenda.Evaluacion.Application.Evento.Queries.BuscarEventoPorId
{
    internal sealed class BuscarEventoPorIdQueryHandler : IQueryHandler<BuscarEventoPorIdQuery, EventoViewModel>
    {
        private readonly IEventoRepository _eventoRepository;

        public BuscarEventoPorIdQueryHandler(IEventoRepository eventoRepository) => _eventoRepository = eventoRepository;

        public async Task<EventoViewModel> Handle(
            BuscarEventoPorIdQuery request, 
            CancellationToken cancellationToken
        )
        {
            var evento = await _eventoRepository.GetByIdAsync(request.IdEvento, cancellationToken);

            if (evento == null)
                throw new EventoNoEncontradoException();

            return evento.Adapt<EventoViewModel>();
        }
    }
}