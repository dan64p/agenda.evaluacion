﻿using System.Collections.Generic;
using ApplicationException = Agenda.Evaluacion.Domain.Exceptions.ApplicationException;

namespace Agenda.Evaluacion.Application.Exceptions
{
    public sealed class ValidationException : ApplicationException
    {
        public ValidationException(IReadOnlyDictionary<string, string[]> errorsDictionary)
            : base("Error de Validación", "Una o más validaciones no han sido cumplidas.")
            => ErrorsDictionary = errorsDictionary;

        public IReadOnlyDictionary<string, string[]> ErrorsDictionary { get; }
    }
}
