﻿using Agenda.Evaluacion.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    internal sealed class AgendaEventoConfiguration : IEntityTypeConfiguration<AgendaEvento>
    {
        public void Configure(EntityTypeBuilder<AgendaEvento> builder)
        {
            builder.ToTable(nameof(AgendaEvento));

            builder.HasKey(user => user.IdAgendaEvento);

            builder.HasOne(p => p.Agenda)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(p => p.IdAgenda)
                .IsRequired();

            builder.HasOne(p => p.Evento)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(p => p.IdEvento)
                .IsRequired();

            builder.HasOne(p => p.InvitacionEvento)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(p => p.IdInvitacionEvento);
        }
    }
}
