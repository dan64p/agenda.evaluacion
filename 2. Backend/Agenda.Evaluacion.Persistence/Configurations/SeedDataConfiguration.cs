﻿using Agenda.Evaluacion.Domain.Entities;
using AgendaEntidad = Agenda.Evaluacion.Domain.Entities.Agenda;
using Agenda.Evaluacion.Domain.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Persistence.Configurations
{
    public static class SeedDataConfiguration
    {
        public static string NombreRol_UsuarioEstandar = "UsuarioEstandar";

        public static string NombreNormalizadoRol_UsuarioEstandar = "USUARIOESTANDAR";
        
        public static string IdRol_UsuarioEstandar = "5a0a563d-936b-4152-af1b-27f8526e10d7";
        
        public static readonly IList<UsuarioSeed> UsuariosPorDefecto = new List<UsuarioSeed> {
            new UsuarioSeed {
                Id = "1ec0e447-6149-4889-a039-2420a1140b52",
                IdAgenda = 1,
                NombreCompleto = "Juan Perez",
                Email = "juan.perez@test.com",
                UserName = "juan.perez@test.com",
                Password = "Desarrollo",
            },
            new UsuarioSeed {
                Id = "430d4714-edbb-49a3-92cf-bf1b006c4157",
                IdAgenda = 2,
                NombreCompleto = "Renata Gomez",
                Email = "renata.gomez@test.com",
                UserName = "renata.gomez@test.com",
                Password = "Desarrollo",
            },
            new UsuarioSeed {
                Id = "296b227c-5adf-4acc-a8d6-45da306f618f",
                IdAgenda = 3,
                NombreCompleto = "Ricardo Mendoza",
                Email = "ricardo.mendoza@test.com",
                UserName = "ricardo.mendoza@test.com",
                Password = "Desarrollo",
            },
            new UsuarioSeed {
                Id = "357058ed-ec2a-42e0-90c6-f21c56e0a124",
                IdAgenda = 4,
                NombreCompleto = "Rosalia Arteaga",
                Email = "rosalia.arteaga@test.com",
                UserName = "rosalia.arteaga@test.com",
                Password = "Desarrollo",
            },
            new UsuarioSeed {
                Id = "70f7b403-6039-434e-912b-489d2138d688",
                IdAgenda = 5,
                NombreCompleto = "Fernando Techera",
                Email = "fernando.techera@test.com",
                UserName = "fernando.techera@test.com",
                Password = "Desarrollo",
            }
        };

        public static readonly IList<TipoEventoSeed> TiposEventosPorDefecto = new List<TipoEventoSeed> { 
            new TipoEventoSeed { Id = 1, Nombre = "Exclusivo", EsCompartible = false },
            new TipoEventoSeed { Id = 2, Nombre = "Compartido", EsCompartible = true }
        };

        public static void SeedData(ModelBuilder builder)
        {
            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Name = NombreRol_UsuarioEstandar,
                NormalizedName = NombreNormalizadoRol_UsuarioEstandar,
                Id = IdRol_UsuarioEstandar,
                ConcurrencyStamp = IdRol_UsuarioEstandar
            });

            var usuariosParaAgregar = new List<Usuario>();
            foreach (var usuarioSeed in UsuariosPorDefecto)
            {
                var usuario = new Usuario
                {
                    Id = usuarioSeed.Id,
                    Email = usuarioSeed.Email,
                    EmailConfirmed = true,
                    UserName = usuarioSeed.UserName,
                    NombreCompleto = usuarioSeed.NombreCompleto,
                    NormalizedUserName = usuarioSeed.UserName.ToUpper()
                };

                PasswordHasher<Usuario> ph = new PasswordHasher<Usuario>();
                usuario.PasswordHash = ph.HashPassword(usuario, usuarioSeed.Password);

                usuariosParaAgregar.Add(usuario);
            }

            builder.Entity<Usuario>().HasData(usuariosParaAgregar);

            builder.Entity<IdentityUserRole<string>>().HasData(
                usuariosParaAgregar.Select(u => new IdentityUserRole<string> {
                    RoleId = IdRol_UsuarioEstandar,
                    UserId = u.Id
                })
            );

            builder.Entity<TipoEvento>()
                .HasData(TiposEventosPorDefecto
                        .Select(te => new TipoEvento(te.Id, te.Nombre, te.EsCompartible))
                );

            builder.Entity<AgendaEntidad>()
                .HasData(UsuariosPorDefecto
                            .Select(u => new AgendaEntidad(u.IdAgenda, "Agenda 1", "Agenda por defecto", u.Id))
                );
        }

        public class UsuarioSeed
        {
            public string Id { get; set; } = string.Empty;
            public int IdAgenda { get; set; }
            public string NombreCompleto { get; set; } = string.Empty;
            public string Email { get; set; } = string.Empty;
            public string UserName { get; set; } = string.Empty;
            public string Password { get; set; } = string.Empty;
        }

        public class TipoEventoSeed
        {
            public int Id { get; set; }
            public string Nombre { get; set; } = string.Empty;
            public bool EsCompartible { get; set; }
        }
    }
}
