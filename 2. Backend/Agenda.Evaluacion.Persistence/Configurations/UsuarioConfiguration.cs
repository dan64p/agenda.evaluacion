﻿using Agenda.Evaluacion.Domain.Entities;
using Agenda.Evaluacion.Domain.Entities.Identity;
using Agenda.Evaluacion.Persistence.Configurations;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    internal sealed class UsuarioConfiguration : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.Property(user => user.NombreCompleto).HasMaxLength(300).IsRequired();                       
        }
    }
}
