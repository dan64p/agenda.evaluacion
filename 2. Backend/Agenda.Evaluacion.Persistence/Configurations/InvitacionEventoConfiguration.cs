﻿using Agenda.Evaluacion.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    internal sealed class InvitacionEventoConfiguration : IEntityTypeConfiguration<InvitacionEvento>
    {
        public void Configure(EntityTypeBuilder<InvitacionEvento> builder)
        {
            builder.ToTable(nameof(InvitacionEvento));

            builder.HasKey(user => user.IdEvento);

            builder.Property(user => user.FueAtendida)
                .IsRequired().HasDefaultValue(false);

            builder.Property(user => user.FueAceptada)
                .IsRequired().HasDefaultValue(false);

            builder.Property(user => user.FechaRegistro)
                .IsRequired().HasDefaultValue(DateTime.Now);

            builder.HasOne(p => p.Evento)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(p => p.IdEvento)
                .IsRequired();

            builder.HasOne(p => p.UsuarioInvitado)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(p => p.IdUsuarioInvitado)
                .IsRequired();

            builder.HasOne(p => p.UsuarioRegistro)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(p => p.IdUsuarioRegistro)
                .IsRequired();
        }
    }
}
