﻿using Agenda.Evaluacion.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    internal sealed class EventoConfiguration : IEntityTypeConfiguration<Evento>
    {
        public void Configure(EntityTypeBuilder<Evento> builder)
        {
            builder.ToTable(nameof(Evento));

            builder.HasKey(user => user.IdEvento);

            builder.Property(user => user.IdEvento).ValueGeneratedOnAdd();

            builder.Property(user => user.Nombre).IsRequired().HasMaxLength(300);

            builder.Property(user => user.Descripcion).IsRequired().HasMaxLength(500);

            builder.Property(user => user.FechaInicio).IsRequired();

            builder.Property(user => user.HoraInicio).IsRequired();

            builder.Property(user => user.FechaFin).IsRequired();

            builder.Property(user => user.HoraFin).IsRequired();

            builder.Property(user => user.FechaRegistro)
                .IsRequired().HasDefaultValue(DateTime.Now);

            builder.HasOne(p => p.TipoEvento)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(p => p.IdTipoEvento)
                .IsRequired();

            builder.HasOne(p => p.UsuarioCreador)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(p => p.IdUsuarioCreador)
                .IsRequired();

            builder.OwnsMany(p => p.Participantes, a =>
            {
                a.WithOwner().HasForeignKey("IdEvento");
                a.Property<int>("IdParticipanteEvento");
                a.HasKey("IdParticipanteEvento");
                a.Property(p => p.NombreCompleto).IsRequired().HasMaxLength(300);
            });

            builder.Property(evento => evento.FechaHoraInicio)
                .HasComputedColumnSql("CONVERT(DATETIME, [FechaInicio]) + CONVERT(DATETIME, [HoraInicio])", true);

            builder.Property(evento => evento.FechaHoraFin)
                .HasComputedColumnSql("CONVERT(DATETIME, [FechaFin]) + CONVERT(DATETIME, [HoraFin])", true);
        }
    }
}
