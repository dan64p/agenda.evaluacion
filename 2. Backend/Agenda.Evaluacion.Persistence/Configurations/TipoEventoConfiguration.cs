﻿using Agenda.Evaluacion.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    internal sealed class TipoEventoConfiguration : IEntityTypeConfiguration<TipoEvento>
    {
        public void Configure(EntityTypeBuilder<TipoEvento> builder)
        {
            builder.ToTable(nameof(TipoEvento));

            builder.HasKey(user => user.IdTipoEvento);

            builder.Property(user => user.Nombre).IsRequired().HasMaxLength(300);

            builder.Property(user => user.EsCompartible).IsRequired();
        }
    }
}
