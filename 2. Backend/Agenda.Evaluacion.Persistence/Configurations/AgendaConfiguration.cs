﻿using Agenda.Evaluacion.Domain.Entities;
using AgendaEntidad = Agenda.Evaluacion.Domain.Entities.Agenda;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    internal sealed class AgendaConfiguration : IEntityTypeConfiguration<AgendaEntidad>
    {
        public void Configure(EntityTypeBuilder<AgendaEntidad> builder)
        {
            builder.ToTable(nameof(AgendaEntidad));

            builder.HasKey(user => user.IdAgenda);

            builder.Property(user => user.Nombre).IsRequired().HasMaxLength(300);

            builder.Property(user => user.Descripcion).HasMaxLength(500);

            builder.HasOne(p => p.Usuario)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasForeignKey(p => p.IdUsuario)
                    .IsRequired();
        }
    }
}
