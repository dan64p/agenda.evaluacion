﻿// <auto-generated />
using System;
using Agenda.Evaluacion.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace Agenda.Evaluacion.Persistence.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("Agenda.Evaluacion.Domain.Entities.Agenda", b =>
                {
                    b.Property<int>("IdAgenda")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdAgenda"));

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(500)
                        .HasColumnType("nvarchar(500)");

                    b.Property<string>("IdUsuario")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasMaxLength(300)
                        .HasColumnType("nvarchar(300)");

                    b.HasKey("IdAgenda");

                    b.HasIndex("IdUsuario");

                    b.ToTable("AgendaEntidad", (string)null);

                    b.HasData(
                        new
                        {
                            IdAgenda = 1,
                            Descripcion = "Agenda por defecto",
                            IdUsuario = "1ec0e447-6149-4889-a039-2420a1140b52",
                            Nombre = "Agenda 1"
                        },
                        new
                        {
                            IdAgenda = 2,
                            Descripcion = "Agenda por defecto",
                            IdUsuario = "430d4714-edbb-49a3-92cf-bf1b006c4157",
                            Nombre = "Agenda 1"
                        },
                        new
                        {
                            IdAgenda = 3,
                            Descripcion = "Agenda por defecto",
                            IdUsuario = "296b227c-5adf-4acc-a8d6-45da306f618f",
                            Nombre = "Agenda 1"
                        },
                        new
                        {
                            IdAgenda = 4,
                            Descripcion = "Agenda por defecto",
                            IdUsuario = "357058ed-ec2a-42e0-90c6-f21c56e0a124",
                            Nombre = "Agenda 1"
                        },
                        new
                        {
                            IdAgenda = 5,
                            Descripcion = "Agenda por defecto",
                            IdUsuario = "70f7b403-6039-434e-912b-489d2138d688",
                            Nombre = "Agenda 1"
                        });
                });

            modelBuilder.Entity("Agenda.Evaluacion.Domain.Entities.AgendaEvento", b =>
                {
                    b.Property<int>("IdAgendaEvento")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdAgendaEvento"));

                    b.Property<int>("IdAgenda")
                        .HasColumnType("int");

                    b.Property<int>("IdEvento")
                        .HasColumnType("int");

                    b.Property<int?>("IdInvitacionEvento")
                        .HasColumnType("int");

                    b.HasKey("IdAgendaEvento");

                    b.HasIndex("IdAgenda");

                    b.HasIndex("IdEvento");

                    b.HasIndex("IdInvitacionEvento");

                    b.ToTable("AgendaEvento", (string)null);
                });

            modelBuilder.Entity("Agenda.Evaluacion.Domain.Entities.Evento", b =>
                {
                    b.Property<int>("IdEvento")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdEvento"));

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(500)
                        .HasColumnType("nvarchar(500)");

                    b.Property<DateTime>("FechaFin")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("FechaHoraFin")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnType("datetime2")
                        .HasComputedColumnSql("CONVERT(DATETIME, [FechaFin]) + CONVERT(DATETIME, [HoraFin])", true);

                    b.Property<DateTime>("FechaHoraInicio")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnType("datetime2")
                        .HasComputedColumnSql("CONVERT(DATETIME, [FechaInicio]) + CONVERT(DATETIME, [HoraInicio])", true);

                    b.Property<DateTime>("FechaInicio")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("FechaRegistro")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValue(new DateTime(2023, 7, 6, 19, 6, 56, 762, DateTimeKind.Local).AddTicks(4538));

                    b.Property<TimeSpan>("HoraFin")
                        .HasColumnType("time");

                    b.Property<TimeSpan>("HoraInicio")
                        .HasColumnType("time");

                    b.Property<int>("IdTipoEvento")
                        .HasColumnType("int");

                    b.Property<string>("IdUsuarioCreador")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Local")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasMaxLength(300)
                        .HasColumnType("nvarchar(300)");

                    b.HasKey("IdEvento");

                    b.HasIndex("IdTipoEvento");

                    b.HasIndex("IdUsuarioCreador");

                    b.ToTable("Evento", (string)null);
                });

            modelBuilder.Entity("Agenda.Evaluacion.Domain.Entities.Identity.Usuario", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NombreCompleto")
                        .IsRequired()
                        .HasMaxLength(300)
                        .HasColumnType("nvarchar(300)");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasDatabaseName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasDatabaseName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers", (string)null);

                    b.HasData(
                        new
                        {
                            Id = "1ec0e447-6149-4889-a039-2420a1140b52",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "60afa623-7f66-4cec-867d-a98890ef8878",
                            Email = "juan.perez@test.com",
                            EmailConfirmed = true,
                            LockoutEnabled = false,
                            NombreCompleto = "Juan Perez",
                            NormalizedUserName = "JUAN.PEREZ@TEST.COM",
                            PasswordHash = "AQAAAAEAACcQAAAAEA43pvf9ZLKt2WJUbwle7BzV35KrBnKydyJXBpEgrFfRjdwIHx7VmTX5Jut/VPAIiw==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "5b05db06-8fb3-41be-b727-c100bf0ff34f",
                            TwoFactorEnabled = false,
                            UserName = "juan.perez@test.com"
                        },
                        new
                        {
                            Id = "430d4714-edbb-49a3-92cf-bf1b006c4157",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "88d24169-c479-4e07-bbb3-0daaa504b641",
                            Email = "renata.gomez@test.com",
                            EmailConfirmed = true,
                            LockoutEnabled = false,
                            NombreCompleto = "Renata Gomez",
                            NormalizedUserName = "RENATA.GOMEZ@TEST.COM",
                            PasswordHash = "AQAAAAEAACcQAAAAEFDKukDfpwYcO2Y00deenLozjL3FoSXyIh6iSv9kxwfmVmyiO9wwKuCpCE4cJUyARA==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "cadfe2d8-309d-484b-bdaa-6494dba375a2",
                            TwoFactorEnabled = false,
                            UserName = "renata.gomez@test.com"
                        },
                        new
                        {
                            Id = "296b227c-5adf-4acc-a8d6-45da306f618f",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "f9142360-86ca-4c8d-af59-be0c2e16e176",
                            Email = "ricardo.mendoza@test.com",
                            EmailConfirmed = true,
                            LockoutEnabled = false,
                            NombreCompleto = "Ricardo Mendoza",
                            NormalizedUserName = "RICARDO.MENDOZA@TEST.COM",
                            PasswordHash = "AQAAAAEAACcQAAAAENSBSwBrDfXLq2tarWdShmxGAgxZq64fgIMEwRV8/A57mi0bbVL40wYtQkR5kVxUcA==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "d45d3b9a-0a51-4061-8ec4-ba255294aed9",
                            TwoFactorEnabled = false,
                            UserName = "ricardo.mendoza@test.com"
                        },
                        new
                        {
                            Id = "357058ed-ec2a-42e0-90c6-f21c56e0a124",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "5df6bbab-d470-49c2-83f8-c2580dcfae3d",
                            Email = "rosalia.arteaga@test.com",
                            EmailConfirmed = true,
                            LockoutEnabled = false,
                            NombreCompleto = "Rosalia Arteaga",
                            NormalizedUserName = "ROSALIA.ARTEAGA@TEST.COM",
                            PasswordHash = "AQAAAAEAACcQAAAAEIng2oIxQVuTPyHNpHMxemmeEMUP8c2xhYLD4THdH1hn6/rWkRK97rx0Sck/dHUNoQ==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "3c83ef1d-15a9-46df-afc0-9619ea18ee05",
                            TwoFactorEnabled = false,
                            UserName = "rosalia.arteaga@test.com"
                        },
                        new
                        {
                            Id = "70f7b403-6039-434e-912b-489d2138d688",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "3c02f97c-e4dd-41d5-a94f-9a021ca0b388",
                            Email = "fernando.techera@test.com",
                            EmailConfirmed = true,
                            LockoutEnabled = false,
                            NombreCompleto = "Fernando Techera",
                            NormalizedUserName = "FERNANDO.TECHERA@TEST.COM",
                            PasswordHash = "AQAAAAEAACcQAAAAEDGTsAVWbVIH/JWfF++N4yhDbB4Bhl2ksbjyxTCp6fe3wVnVqEPOQm9FW9NIfMjslg==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "7f52be54-6098-40e6-88e1-7e7d74d0894d",
                            TwoFactorEnabled = false,
                            UserName = "fernando.techera@test.com"
                        });
                });

            modelBuilder.Entity("Agenda.Evaluacion.Domain.Entities.InvitacionEvento", b =>
                {
                    b.Property<int>("IdEvento")
                        .HasColumnType("int");

                    b.Property<DateTime>("FechaRegistro")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValue(new DateTime(2023, 7, 6, 19, 6, 56, 767, DateTimeKind.Local).AddTicks(5714));

                    b.Property<bool>("FueAceptada")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bit")
                        .HasDefaultValue(false);

                    b.Property<bool>("FueAtendida")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bit")
                        .HasDefaultValue(false);

                    b.Property<int>("IdInvitacionEvento")
                        .HasColumnType("int");

                    b.Property<string>("IdUsuarioInvitado")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("IdUsuarioRegistro")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("IdEvento");

                    b.HasIndex("IdUsuarioInvitado");

                    b.HasIndex("IdUsuarioRegistro");

                    b.ToTable("InvitacionEvento", (string)null);
                });

            modelBuilder.Entity("Agenda.Evaluacion.Domain.Entities.TipoEvento", b =>
                {
                    b.Property<int>("IdTipoEvento")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdTipoEvento"));

                    b.Property<bool>("EsCompartible")
                        .HasColumnType("bit");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasMaxLength(300)
                        .HasColumnType("nvarchar(300)");

                    b.HasKey("IdTipoEvento");

                    b.ToTable("TipoEvento", (string)null);

                    b.HasData(
                        new
                        {
                            IdTipoEvento = 1,
                            EsCompartible = false,
                            Nombre = "Exclusivo"
                        },
                        new
                        {
                            IdTipoEvento = 2,
                            EsCompartible = true,
                            Nombre = "Compartido"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasDatabaseName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles", (string)null);

                    b.HasData(
                        new
                        {
                            Id = "5a0a563d-936b-4152-af1b-27f8526e10d7",
                            ConcurrencyStamp = "5a0a563d-936b-4152-af1b-27f8526e10d7",
                            Name = "UsuarioEstandar",
                            NormalizedName = "USUARIOESTANDAR"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("RoleId")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles", (string)null);

                    b.HasData(
                        new
                        {
                            UserId = "1ec0e447-6149-4889-a039-2420a1140b52",
                            RoleId = "5a0a563d-936b-4152-af1b-27f8526e10d7"
                        },
                        new
                        {
                            UserId = "430d4714-edbb-49a3-92cf-bf1b006c4157",
                            RoleId = "5a0a563d-936b-4152-af1b-27f8526e10d7"
                        },
                        new
                        {
                            UserId = "296b227c-5adf-4acc-a8d6-45da306f618f",
                            RoleId = "5a0a563d-936b-4152-af1b-27f8526e10d7"
                        },
                        new
                        {
                            UserId = "357058ed-ec2a-42e0-90c6-f21c56e0a124",
                            RoleId = "5a0a563d-936b-4152-af1b-27f8526e10d7"
                        },
                        new
                        {
                            UserId = "70f7b403-6039-434e-912b-489d2138d688",
                            RoleId = "5a0a563d-936b-4152-af1b-27f8526e10d7"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens", (string)null);
                });

            modelBuilder.Entity("Agenda.Evaluacion.Domain.Entities.Agenda", b =>
                {
                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Identity.Usuario", "Usuario")
                        .WithMany()
                        .HasForeignKey("IdUsuario")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.Navigation("Usuario");
                });

            modelBuilder.Entity("Agenda.Evaluacion.Domain.Entities.AgendaEvento", b =>
                {
                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Agenda", "Agenda")
                        .WithMany()
                        .HasForeignKey("IdAgenda")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Evento", "Evento")
                        .WithMany()
                        .HasForeignKey("IdEvento")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Agenda.Evaluacion.Domain.Entities.InvitacionEvento", "InvitacionEvento")
                        .WithMany()
                        .HasForeignKey("IdInvitacionEvento")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.Navigation("Agenda");

                    b.Navigation("Evento");

                    b.Navigation("InvitacionEvento");
                });

            modelBuilder.Entity("Agenda.Evaluacion.Domain.Entities.Evento", b =>
                {
                    b.HasOne("Agenda.Evaluacion.Domain.Entities.TipoEvento", "TipoEvento")
                        .WithMany()
                        .HasForeignKey("IdTipoEvento")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Identity.Usuario", "UsuarioCreador")
                        .WithMany()
                        .HasForeignKey("IdUsuarioCreador")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.OwnsMany("Agenda.Evaluacion.Domain.Entities.ParticipanteEvento", "Participantes", b1 =>
                        {
                            b1.Property<int>("IdParticipanteEvento")
                                .ValueGeneratedOnAdd()
                                .HasColumnType("int");

                            SqlServerPropertyBuilderExtensions.UseIdentityColumn(b1.Property<int>("IdParticipanteEvento"));

                            b1.Property<int>("IdEvento")
                                .HasColumnType("int");

                            b1.Property<int?>("IdUsuario")
                                .HasColumnType("int");

                            b1.Property<string>("NombreCompleto")
                                .IsRequired()
                                .HasMaxLength(300)
                                .HasColumnType("nvarchar(300)");

                            b1.Property<string>("UsuarioId")
                                .HasColumnType("nvarchar(450)");

                            b1.HasKey("IdParticipanteEvento");

                            b1.HasIndex("IdEvento");

                            b1.HasIndex("UsuarioId");

                            b1.ToTable("ParticipanteEvento");

                            b1.WithOwner()
                                .HasForeignKey("IdEvento");

                            b1.HasOne("Agenda.Evaluacion.Domain.Entities.Identity.Usuario", "Usuario")
                                .WithMany()
                                .HasForeignKey("UsuarioId");

                            b1.Navigation("Usuario");
                        });

                    b.Navigation("Participantes");

                    b.Navigation("TipoEvento");

                    b.Navigation("UsuarioCreador");
                });

            modelBuilder.Entity("Agenda.Evaluacion.Domain.Entities.InvitacionEvento", b =>
                {
                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Evento", "Evento")
                        .WithMany()
                        .HasForeignKey("IdEvento")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Identity.Usuario", "UsuarioInvitado")
                        .WithMany()
                        .HasForeignKey("IdUsuarioInvitado")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Identity.Usuario", "UsuarioRegistro")
                        .WithMany()
                        .HasForeignKey("IdUsuarioRegistro")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.Navigation("Evento");

                    b.Navigation("UsuarioInvitado");

                    b.Navigation("UsuarioRegistro");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Identity.Usuario", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Identity.Usuario", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Identity.Usuario", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("Agenda.Evaluacion.Domain.Entities.Identity.Usuario", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
