﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Agenda.Evaluacion.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    NombreCompleto = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoEvento",
                columns: table => new
                {
                    IdTipoEvento = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    EsCompartible = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoEvento", x => x.IdTipoEvento);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AgendaEntidad",
                columns: table => new
                {
                    IdAgenda = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    Descripcion = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    IdUsuario = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgendaEntidad", x => x.IdAgenda);
                    table.ForeignKey(
                        name: "FK_AgendaEntidad_AspNetUsers_IdUsuario",
                        column: x => x.IdUsuario,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Evento",
                columns: table => new
                {
                    IdEvento = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    Descripcion = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    FechaInicio = table.Column<DateTime>(type: "datetime2", nullable: false),
                    HoraInicio = table.Column<TimeSpan>(type: "time", nullable: false),
                    FechaFin = table.Column<DateTime>(type: "datetime2", nullable: false),
                    HoraFin = table.Column<TimeSpan>(type: "time", nullable: false),
                    Local = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IdTipoEvento = table.Column<int>(type: "int", nullable: false),
                    IdUsuarioCreador = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    FechaRegistro = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2023, 7, 5, 2, 35, 38, 132, DateTimeKind.Local).AddTicks(2167))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Evento", x => x.IdEvento);
                    table.ForeignKey(
                        name: "FK_Evento_AspNetUsers_IdUsuarioCreador",
                        column: x => x.IdUsuarioCreador,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Evento_TipoEvento_IdTipoEvento",
                        column: x => x.IdTipoEvento,
                        principalTable: "TipoEvento",
                        principalColumn: "IdTipoEvento",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InvitacionEvento",
                columns: table => new
                {
                    IdEvento = table.Column<int>(type: "int", nullable: false),
                    IdInvitacionEvento = table.Column<int>(type: "int", nullable: false),
                    FueAtendida = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    FueAceptada = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    IdUsuarioInvitado = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    IdUsuarioRegistro = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    FechaRegistro = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2023, 7, 5, 2, 35, 38, 135, DateTimeKind.Local).AddTicks(4073))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvitacionEvento", x => x.IdEvento);
                    table.ForeignKey(
                        name: "FK_InvitacionEvento_AspNetUsers_IdUsuarioInvitado",
                        column: x => x.IdUsuarioInvitado,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvitacionEvento_AspNetUsers_IdUsuarioRegistro",
                        column: x => x.IdUsuarioRegistro,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvitacionEvento_Evento_IdEvento",
                        column: x => x.IdEvento,
                        principalTable: "Evento",
                        principalColumn: "IdEvento",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ParticipanteEvento",
                columns: table => new
                {
                    IdParticipanteEvento = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NombreCompleto = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    IdUsuario = table.Column<int>(type: "int", nullable: true),
                    UsuarioId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    IdEvento = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParticipanteEvento", x => x.IdParticipanteEvento);
                    table.ForeignKey(
                        name: "FK_ParticipanteEvento_AspNetUsers_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ParticipanteEvento_Evento_IdEvento",
                        column: x => x.IdEvento,
                        principalTable: "Evento",
                        principalColumn: "IdEvento",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AgendaEvento",
                columns: table => new
                {
                    IdAgendaEvento = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdAgenda = table.Column<int>(type: "int", nullable: false),
                    IdEvento = table.Column<int>(type: "int", nullable: false),
                    IdInvitacionEvento = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgendaEvento", x => x.IdAgendaEvento);
                    table.ForeignKey(
                        name: "FK_AgendaEvento_AgendaEntidad_IdAgenda",
                        column: x => x.IdAgenda,
                        principalTable: "AgendaEntidad",
                        principalColumn: "IdAgenda",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgendaEvento_Evento_IdEvento",
                        column: x => x.IdEvento,
                        principalTable: "Evento",
                        principalColumn: "IdEvento",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgendaEvento_InvitacionEvento_IdInvitacionEvento",
                        column: x => x.IdInvitacionEvento,
                        principalTable: "InvitacionEvento",
                        principalColumn: "IdEvento",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "5a0a563d-936b-4152-af1b-27f8526e10d7", "5a0a563d-936b-4152-af1b-27f8526e10d7", "UsuarioEstandar", "USUARIOESTANDAR" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NombreCompleto", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "1ec0e447-6149-4889-a039-2420a1140b52", 0, "ac20f320-a8f5-47ff-a197-2d74fedb01c1", "juan.perez@test.com", true, false, null, "", null, "JUAN.PEREZ@TEST.COM", "AQAAAAEAACcQAAAAEH7gVqS2QqklQf+RoT6HNkhMyJxnTV7Ao28YxVEVZ8MvJM0ThqOf8hYNmaHjquxycQ==", null, false, "889fc9c8-532d-4c55-94be-0f4c3fef96ae", false, "juan.perez@test.com" },
                    { "296b227c-5adf-4acc-a8d6-45da306f618f", 0, "9966a0f6-749d-4d29-8c70-e4fe52140bd0", "ricardo.mendoza@test.com", true, false, null, "", null, "RICARDO.MENDOZA@TEST.COM", "AQAAAAEAACcQAAAAELBJq5E6fMVbFzJtUP/4E/05GPVj/okeqPpqCUm28ziUFUN6ISOqQD+2LLEfOvOohw==", null, false, "39b3e73b-f4df-4056-9834-33b5585ca776", false, "ricardo.mendoza@test.com" },
                    { "357058ed-ec2a-42e0-90c6-f21c56e0a124", 0, "768ef77c-85d9-4bec-bb0d-6cf05f6d61b6", "rosalia.arteaga@test.com", true, false, null, "", null, "ROSALIA.ARTEAGA@TEST.COM", "AQAAAAEAACcQAAAAENAKYpbmO0hkRvbYH90y82OOGrOXbtab38wJ/ffMtnVDgNhPsUn6W9yi92jm1bHIRw==", null, false, "c5ce12d1-fced-455d-98ad-a305860cd3e5", false, "rosalia.arteaga@test.com" },
                    { "430d4714-edbb-49a3-92cf-bf1b006c4157", 0, "0784083a-9d2b-4271-8277-bb293960ce41", "renata.gomez@test.com", true, false, null, "", null, "RENATA.GOMEZ@TEST.COM", "AQAAAAEAACcQAAAAEJ4z5vwEqyzlBCKZDFswcYP1hJxuZVWsv91g5UqW+7AHtN3oEQqEkQ8/81g6TB6pVg==", null, false, "f956df60-fcc2-404b-b119-f95f5ff34bb3", false, "renata.gomez@test.com" },
                    { "70f7b403-6039-434e-912b-489d2138d688", 0, "fe0de8e4-b2f6-4ceb-a78f-36b0f0c578d9", "fernando.techera@test.com", true, false, null, "", null, "FERNANDO.TECHERA@TEST.COM", "AQAAAAEAACcQAAAAELdIo3Hv9d4n9MCrL7E6puii+jk0LtQSQiSDKVs9OrIAnHBk9vNDvpB87iO6sz5o5g==", null, false, "7ff1ba48-a0d9-40b8-9794-8a1594c93f1d", false, "fernando.techera@test.com" }
                });

            migrationBuilder.InsertData(
                table: "TipoEvento",
                columns: new[] { "IdTipoEvento", "EsCompartible", "Nombre" },
                values: new object[,]
                {
                    { 1, false, "Exclusivo" },
                    { 2, true, "Compartido" }
                });

            migrationBuilder.InsertData(
                table: "AgendaEntidad",
                columns: new[] { "IdAgenda", "Descripcion", "IdUsuario", "Nombre" },
                values: new object[,]
                {
                    { 1, "Agenda por defecto", "1ec0e447-6149-4889-a039-2420a1140b52", "Agenda 1" },
                    { 2, "Agenda por defecto", "430d4714-edbb-49a3-92cf-bf1b006c4157", "Agenda 1" },
                    { 3, "Agenda por defecto", "296b227c-5adf-4acc-a8d6-45da306f618f", "Agenda 1" },
                    { 4, "Agenda por defecto", "357058ed-ec2a-42e0-90c6-f21c56e0a124", "Agenda 1" },
                    { 5, "Agenda por defecto", "70f7b403-6039-434e-912b-489d2138d688", "Agenda 1" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "5a0a563d-936b-4152-af1b-27f8526e10d7", "1ec0e447-6149-4889-a039-2420a1140b52" },
                    { "5a0a563d-936b-4152-af1b-27f8526e10d7", "296b227c-5adf-4acc-a8d6-45da306f618f" },
                    { "5a0a563d-936b-4152-af1b-27f8526e10d7", "357058ed-ec2a-42e0-90c6-f21c56e0a124" },
                    { "5a0a563d-936b-4152-af1b-27f8526e10d7", "430d4714-edbb-49a3-92cf-bf1b006c4157" },
                    { "5a0a563d-936b-4152-af1b-27f8526e10d7", "70f7b403-6039-434e-912b-489d2138d688" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AgendaEntidad_IdUsuario",
                table: "AgendaEntidad",
                column: "IdUsuario");

            migrationBuilder.CreateIndex(
                name: "IX_AgendaEvento_IdAgenda",
                table: "AgendaEvento",
                column: "IdAgenda");

            migrationBuilder.CreateIndex(
                name: "IX_AgendaEvento_IdEvento",
                table: "AgendaEvento",
                column: "IdEvento");

            migrationBuilder.CreateIndex(
                name: "IX_AgendaEvento_IdInvitacionEvento",
                table: "AgendaEvento",
                column: "IdInvitacionEvento");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Evento_IdTipoEvento",
                table: "Evento",
                column: "IdTipoEvento");

            migrationBuilder.CreateIndex(
                name: "IX_Evento_IdUsuarioCreador",
                table: "Evento",
                column: "IdUsuarioCreador");

            migrationBuilder.CreateIndex(
                name: "IX_InvitacionEvento_IdUsuarioInvitado",
                table: "InvitacionEvento",
                column: "IdUsuarioInvitado");

            migrationBuilder.CreateIndex(
                name: "IX_InvitacionEvento_IdUsuarioRegistro",
                table: "InvitacionEvento",
                column: "IdUsuarioRegistro");

            migrationBuilder.CreateIndex(
                name: "IX_ParticipanteEvento_IdEvento",
                table: "ParticipanteEvento",
                column: "IdEvento");

            migrationBuilder.CreateIndex(
                name: "IX_ParticipanteEvento_UsuarioId",
                table: "ParticipanteEvento",
                column: "UsuarioId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AgendaEvento");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ParticipanteEvento");

            migrationBuilder.DropTable(
                name: "AgendaEntidad");

            migrationBuilder.DropTable(
                name: "InvitacionEvento");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Evento");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "TipoEvento");
        }
    }
}
