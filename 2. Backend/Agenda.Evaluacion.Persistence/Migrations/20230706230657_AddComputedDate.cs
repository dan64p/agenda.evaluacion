﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Agenda.Evaluacion.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class AddComputedDate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "FechaRegistro",
                table: "InvitacionEvento",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 7, 6, 19, 6, 56, 767, DateTimeKind.Local).AddTicks(5714),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 7, 5, 2, 35, 38, 135, DateTimeKind.Local).AddTicks(4073));

            migrationBuilder.AlterColumn<DateTime>(
                name: "FechaRegistro",
                table: "Evento",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 7, 6, 19, 6, 56, 762, DateTimeKind.Local).AddTicks(4538),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 7, 5, 2, 35, 38, 132, DateTimeKind.Local).AddTicks(2167));

            migrationBuilder.AddColumn<DateTime>(
                name: "FechaHoraFin",
                table: "Evento",
                type: "datetime2",
                nullable: false,
                computedColumnSql: "CONVERT(DATETIME, [FechaFin]) + CONVERT(DATETIME, [HoraFin])",
                stored: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FechaHoraInicio",
                table: "Evento",
                type: "datetime2",
                nullable: false,
                computedColumnSql: "CONVERT(DATETIME, [FechaInicio]) + CONVERT(DATETIME, [HoraInicio])",
                stored: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1ec0e447-6149-4889-a039-2420a1140b52",
                columns: new[] { "ConcurrencyStamp", "NombreCompleto", "PasswordHash", "SecurityStamp" },
                values: new object[] { "60afa623-7f66-4cec-867d-a98890ef8878", "Juan Perez", "AQAAAAEAACcQAAAAEA43pvf9ZLKt2WJUbwle7BzV35KrBnKydyJXBpEgrFfRjdwIHx7VmTX5Jut/VPAIiw==", "5b05db06-8fb3-41be-b727-c100bf0ff34f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "296b227c-5adf-4acc-a8d6-45da306f618f",
                columns: new[] { "ConcurrencyStamp", "NombreCompleto", "PasswordHash", "SecurityStamp" },
                values: new object[] { "f9142360-86ca-4c8d-af59-be0c2e16e176", "Ricardo Mendoza", "AQAAAAEAACcQAAAAENSBSwBrDfXLq2tarWdShmxGAgxZq64fgIMEwRV8/A57mi0bbVL40wYtQkR5kVxUcA==", "d45d3b9a-0a51-4061-8ec4-ba255294aed9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "357058ed-ec2a-42e0-90c6-f21c56e0a124",
                columns: new[] { "ConcurrencyStamp", "NombreCompleto", "PasswordHash", "SecurityStamp" },
                values: new object[] { "5df6bbab-d470-49c2-83f8-c2580dcfae3d", "Rosalia Arteaga", "AQAAAAEAACcQAAAAEIng2oIxQVuTPyHNpHMxemmeEMUP8c2xhYLD4THdH1hn6/rWkRK97rx0Sck/dHUNoQ==", "3c83ef1d-15a9-46df-afc0-9619ea18ee05" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "430d4714-edbb-49a3-92cf-bf1b006c4157",
                columns: new[] { "ConcurrencyStamp", "NombreCompleto", "PasswordHash", "SecurityStamp" },
                values: new object[] { "88d24169-c479-4e07-bbb3-0daaa504b641", "Renata Gomez", "AQAAAAEAACcQAAAAEFDKukDfpwYcO2Y00deenLozjL3FoSXyIh6iSv9kxwfmVmyiO9wwKuCpCE4cJUyARA==", "cadfe2d8-309d-484b-bdaa-6494dba375a2" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "70f7b403-6039-434e-912b-489d2138d688",
                columns: new[] { "ConcurrencyStamp", "NombreCompleto", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3c02f97c-e4dd-41d5-a94f-9a021ca0b388", "Fernando Techera", "AQAAAAEAACcQAAAAEDGTsAVWbVIH/JWfF++N4yhDbB4Bhl2ksbjyxTCp6fe3wVnVqEPOQm9FW9NIfMjslg==", "7f52be54-6098-40e6-88e1-7e7d74d0894d" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FechaHoraFin",
                table: "Evento");

            migrationBuilder.DropColumn(
                name: "FechaHoraInicio",
                table: "Evento");

            migrationBuilder.AlterColumn<DateTime>(
                name: "FechaRegistro",
                table: "InvitacionEvento",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 7, 5, 2, 35, 38, 135, DateTimeKind.Local).AddTicks(4073),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 7, 6, 19, 6, 56, 767, DateTimeKind.Local).AddTicks(5714));

            migrationBuilder.AlterColumn<DateTime>(
                name: "FechaRegistro",
                table: "Evento",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2023, 7, 5, 2, 35, 38, 132, DateTimeKind.Local).AddTicks(2167),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2023, 7, 6, 19, 6, 56, 762, DateTimeKind.Local).AddTicks(4538));

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1ec0e447-6149-4889-a039-2420a1140b52",
                columns: new[] { "ConcurrencyStamp", "NombreCompleto", "PasswordHash", "SecurityStamp" },
                values: new object[] { "ac20f320-a8f5-47ff-a197-2d74fedb01c1", "", "AQAAAAEAACcQAAAAEH7gVqS2QqklQf+RoT6HNkhMyJxnTV7Ao28YxVEVZ8MvJM0ThqOf8hYNmaHjquxycQ==", "889fc9c8-532d-4c55-94be-0f4c3fef96ae" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "296b227c-5adf-4acc-a8d6-45da306f618f",
                columns: new[] { "ConcurrencyStamp", "NombreCompleto", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9966a0f6-749d-4d29-8c70-e4fe52140bd0", "", "AQAAAAEAACcQAAAAELBJq5E6fMVbFzJtUP/4E/05GPVj/okeqPpqCUm28ziUFUN6ISOqQD+2LLEfOvOohw==", "39b3e73b-f4df-4056-9834-33b5585ca776" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "357058ed-ec2a-42e0-90c6-f21c56e0a124",
                columns: new[] { "ConcurrencyStamp", "NombreCompleto", "PasswordHash", "SecurityStamp" },
                values: new object[] { "768ef77c-85d9-4bec-bb0d-6cf05f6d61b6", "", "AQAAAAEAACcQAAAAENAKYpbmO0hkRvbYH90y82OOGrOXbtab38wJ/ffMtnVDgNhPsUn6W9yi92jm1bHIRw==", "c5ce12d1-fced-455d-98ad-a305860cd3e5" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "430d4714-edbb-49a3-92cf-bf1b006c4157",
                columns: new[] { "ConcurrencyStamp", "NombreCompleto", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0784083a-9d2b-4271-8277-bb293960ce41", "", "AQAAAAEAACcQAAAAEJ4z5vwEqyzlBCKZDFswcYP1hJxuZVWsv91g5UqW+7AHtN3oEQqEkQ8/81g6TB6pVg==", "f956df60-fcc2-404b-b119-f95f5ff34bb3" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "70f7b403-6039-434e-912b-489d2138d688",
                columns: new[] { "ConcurrencyStamp", "NombreCompleto", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fe0de8e4-b2f6-4ceb-a78f-36b0f0c578d9", "", "AQAAAAEAACcQAAAAELdIo3Hv9d4n9MCrL7E6puii+jk0LtQSQiSDKVs9OrIAnHBk9vNDvpB87iO6sz5o5g==", "7ff1ba48-a0d9-40b8-9794-8a1594c93f1d" });
        }
    }
}
