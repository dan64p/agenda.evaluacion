﻿using Agenda.Evaluacion.Common.Communication.RequestUtils;
using Agenda.Evaluacion.Common.Communication.ResponseUtils;
using Agenda.Evaluacion.Domain.Entities;
using Agenda.Evaluacion.Domain.Models.Filters;
using Agenda.Evaluacion.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Agenda.Evaluacion.Persistence.Repositories
{
    public class AgendaEventoRepository : IAgendaEventoRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public AgendaEventoRepository(ApplicationDbContext dbContext) => _dbContext = dbContext;

        public async Task<IEnumerable<AgendaEvento>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            return await _dbContext.AgendasEventos.ToListAsync(cancellationToken); 
        }

        public async Task<AgendaEvento> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            var agendaEvento = await _dbContext.AgendasEventos.FirstOrDefaultAsync(agendaEnv => agendaEnv.IdAgendaEvento == id, cancellationToken);

            return agendaEvento;
        }

        public void Insert(AgendaEvento entity)
        {
            _dbContext.AgendasEventos.Add(entity);
        }

        public void Update(AgendaEvento entity)
        {
            _dbContext.AgendasEventos.Update(entity);
        }

        public void Delete(AgendaEvento entity)
        {
            _dbContext.AgendasEventos.Remove(entity);
        }

        public async Task DeleteByEvento(Evento evento)
        {
            var agendasEventos = await _dbContext
                                            .AgendasEventos
                                            .Where(agendaEvento => agendaEvento.IdEvento == evento.IdEvento)
                                            .ToListAsync();

            _dbContext.AgendasEventos.RemoveRange(agendasEventos);
        }

        public async Task<IEnumerable<AgendaEvento>> SearchByFilter(AgendaEventoFiltro filter, CancellationToken cancellationToken = default)
        {
            return await _dbContext.AgendasEventos
                                .WhereAgendaEventoFilter(filter)
                                .OrderByAgendaEventoFilter()
                                .IncludeAgendaEventoFilter()
                                .ToListAsync();
        }

        public async Task<PaginatedResponse<AgendaEvento>> SearchPaginatedByFilter(PaginatedRequest<AgendaEventoFiltro> filter, CancellationToken cancellationToken = default)
        {
            var totalRecords = await _dbContext.AgendasEventos
                                .WhereAgendaEventoFilter(filter.Filter).CountAsync();

             var data = await _dbContext.AgendasEventos
                                .WhereAgendaEventoFilter(filter.Filter)
                                .OrderByAgendaEventoFilter()
                                .IncludeAgendaEventoFilter()
                                .Skip((filter.Page - 1) * filter.PageSize)
                                .Take(filter.PageSize)
                                .ToListAsync();

            return new PaginatedResponse<AgendaEvento>
            {
                Page = filter.Page,
                PageSize = filter.PageSize,
                TotalRecords = totalRecords,
                Data = data
            };
        }

        public async Task<IEnumerable<AgendaEvento>> SearchCalendarioByFilter(AgendaEventoCalendarioFiltro filter, CancellationToken cancellationToken = default)
        {
            return await _dbContext.AgendasEventos
                                .WhereAgendaEventoCalendarioFilter(filter)
                                .OrderByAgendaEventoCalendarioFilter()
                                .Include(p => p.Evento)
                                .ToListAsync();
        }
    }

    internal static class AgendaEventoRepositoryExtensions {
        internal static IQueryable<AgendaEvento> WhereAgendaEventoFilter(this DbSet<AgendaEvento> bdSet, AgendaEventoFiltro filter)
        {
            return bdSet.Where(agendaEvento =>
                                    agendaEvento.Agenda.IdUsuario.Equals(filter.IdUsuario)
                                    &&
                                    (
                                        string.IsNullOrEmpty(filter.FiltroEvento) ||
                                        (agendaEvento.Evento.Nombre.ToUpper().Contains(filter.FiltroEvento) ||
                                        agendaEvento.Evento.Descripcion.ToUpper().Contains(filter.FiltroEvento))
                                    )
                                    &&
                                    (
                                        !filter.FechaEvento.HasValue ||
                                        (filter.FechaEvento >= agendaEvento.Evento.FechaInicio && filter.FechaEvento <= agendaEvento.Evento.FechaFin)
                                    )
                                    &&
                                    (
                                        !filter.HoraEvento.HasValue ||
                                        (filter.HoraEvento >= agendaEvento.Evento.HoraInicio && filter.HoraEvento <= agendaEvento.Evento.HoraFin)
                                    )
                                 );
        }

        internal static IQueryable<AgendaEvento> OrderByAgendaEventoFilter(this IQueryable<AgendaEvento> query)
        {
            return query.OrderByDescending(agendaEvento => agendaEvento.Evento.FechaInicio)
                                .ThenByDescending(agendaEvento => agendaEvento.Evento.HoraInicio);
        }

        internal static IQueryable<AgendaEvento> IncludeAgendaEventoFilter(this IQueryable<AgendaEvento> query)
        {
            return query.Include(p => p.Evento)
                        .ThenInclude(e => e.UsuarioCreador)
                        .Include(p => p.Evento)
                        .ThenInclude(e => e.TipoEvento)
                        .Include(p => p.Agenda);
        }

        internal static IQueryable<AgendaEvento> WhereAgendaEventoCalendarioFilter(this DbSet<AgendaEvento> bdSet, AgendaEventoCalendarioFiltro filter)
        {
            return bdSet.Where(agendaEvento =>
                                    agendaEvento.Agenda.IdUsuario.Equals(filter.IdUsuario)
                                    &&
                                    (
                                        string.IsNullOrEmpty(filter.FiltroEvento) ||
                                        (agendaEvento.Evento.Nombre.ToUpper().Contains(filter.FiltroEvento) ||
                                        agendaEvento.Evento.Descripcion.ToUpper().Contains(filter.FiltroEvento))
                                    )
                                    &&
                                    (
                                        agendaEvento.Evento.FechaInicio.Year == filter.Anio ||
                                        agendaEvento.Evento.FechaFin.Year == filter.Anio
                                    )
                                    &&
                                    (
                                        agendaEvento.Evento.FechaInicio.Month == filter.Mes ||
                                        agendaEvento.Evento.FechaFin.Month == filter.Mes
                                    )
                                    &&
                                    (
                                        !filter.Dia.HasValue ||
                                        agendaEvento.Evento.FechaInicio.Day == filter.Dia ||
                                        agendaEvento.Evento.FechaFin.Day == filter.Dia
                                    )
                                    &&
                                    (   
                                        !filter.Hora.HasValue ||
                                        (filter.Hora >= agendaEvento.Evento.HoraInicio  && filter.Hora <= agendaEvento.Evento.HoraFin)
                                    )
                                 );
        }

        internal static IQueryable<AgendaEvento> OrderByAgendaEventoCalendarioFilter(this IQueryable<AgendaEvento> query)
        {
            return query.OrderBy(agendaEvento => agendaEvento.Evento.FechaHoraInicio);
        }
    }
}
