﻿using Agenda.Evaluacion.Domain.Entities;
using Agenda.Evaluacion.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Agenda.Evaluacion.Persistence.Repositories
{
    public class InvitacionEventoRepository : IInvitacionEventoRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public InvitacionEventoRepository(ApplicationDbContext dbContext) => _dbContext = dbContext;

        public async Task<IEnumerable<InvitacionEvento>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            return await _dbContext.InvitacionesEventos.ToListAsync(cancellationToken);
        }

        public async Task<InvitacionEvento> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            var invitacion = await _dbContext.InvitacionesEventos.FirstOrDefaultAsync(inv => inv.IdInvitacionEvento == id);

            return invitacion;
        }

        public void Insert(InvitacionEvento entity)
        {
            _dbContext.InvitacionesEventos.Add(entity);
        }

        public void Update(InvitacionEvento entity)
        {
            _dbContext.InvitacionesEventos.Update(entity);
        }

        public void Delete(InvitacionEvento entity)
        {
            _dbContext.InvitacionesEventos.Remove(entity);
        }
    }
}
