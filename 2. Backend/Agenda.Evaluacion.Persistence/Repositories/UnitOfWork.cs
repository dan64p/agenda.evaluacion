﻿using Agenda.Evaluacion.Domain.Repositories;
using Microsoft.EntityFrameworkCore.Storage;

namespace Agenda.Evaluacion.Persistence.Repositories
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _dbContext;
        private IDbContextTransaction _dbTransaction;

        public UnitOfWork(ApplicationDbContext dbContext) => _dbContext = dbContext;

        public void EnableMultipleChanges()
        {
            if(_dbTransaction == null)
                _dbTransaction = _dbContext.Database.BeginTransaction();
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken = default) =>
            _dbContext.SaveChangesAsync(cancellationToken);

        public void SaveAllChanges()
        {
            if (_dbTransaction != null)
                _dbTransaction.Commit();
        }

        public void UndoAllChanges()
        {
            if (_dbTransaction != null)
                _dbTransaction.Rollback();
        }

        public void Dispose()
        {
            if (_dbTransaction != null) {
                _dbTransaction.Dispose();
            }
        }
    }
}