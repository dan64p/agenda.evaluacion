﻿using Agenda.Evaluacion.Domain.Entities;
using Agenda.Evaluacion.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Agenda.Evaluacion.Persistence.Repositories
{
    public  sealed class TipoEventoRepository : ITipoEventoRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public TipoEventoRepository(ApplicationDbContext dbContext) => _dbContext = dbContext;

        public async Task<IEnumerable<TipoEvento>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            return await _dbContext.TiposEventos.ToListAsync(cancellationToken);
        }

        public async Task<TipoEvento> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            var evento = await _dbContext.TiposEventos.
                                FirstOrDefaultAsync(evento => evento.IdTipoEvento == id, cancellationToken);

            return evento;
        }

        public void Insert(TipoEvento entity)
        {
            _dbContext.TiposEventos.Add(entity);
        }

        public void Update(TipoEvento entity)
        {
            _dbContext.TiposEventos.Update(entity);
        }

        public void Delete(TipoEvento entity)
        {
            _dbContext.TiposEventos.Remove(entity);
        }
    }
}
