﻿using Agenda.Evaluacion.Domain.Entities;
using Agenda.Evaluacion.Domain.Models.Filters;
using Agenda.Evaluacion.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Agenda.Evaluacion.Persistence.Repositories
{
    public  sealed class EventoRepository : IEventoRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public EventoRepository(ApplicationDbContext dbContext) => _dbContext = dbContext;

        public async Task<IEnumerable<Evento>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Eventos.ToListAsync(cancellationToken);
        }

        public async Task<Evento> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            var evento = await _dbContext.Eventos.
                                FirstOrDefaultAsync(evento => evento.IdEvento == id, cancellationToken);

            return evento;
        }

        public void Insert(Evento entity)
        {
            _dbContext.Eventos.Add(entity);
        }

        public void Update(Evento entity)
        {
            _dbContext.Eventos.Update(entity);
        }

        public void Delete(Evento entity)
        {
            _dbContext.Eventos.Remove(entity);
        }

        public async Task<bool> ExisteEventoExclusivoEntreFechas(ExisteEventoExclusivoFiltro filtro, CancellationToken cancellationToken)
        {
            return await _dbContext.Eventos
                 .Where(evento =>
                    evento.IdEvento != filtro.IdEvento &&
                    evento.IdUsuarioCreador == filtro.IdUsuarioCreador &&
                    (
                      (evento.FechaHoraInicio >= filtro.FechaHoraInicio  && evento.FechaHoraInicio < filtro.FechaHoraFin)
                      ||
                      (evento.FechaHoraFin > filtro.FechaHoraInicio && evento.FechaHoraFin <= filtro.FechaHoraFin)
                      ||
                      (filtro.FechaHoraInicio >= evento.FechaHoraInicio && filtro.FechaHoraInicio < evento.FechaHoraFin)
                      ||
                      (filtro.FechaHoraFin > evento.FechaHoraInicio && filtro.FechaHoraFin <= evento.FechaHoraFin)
                    )
                 )
                .CountAsync() > 0;
        }
    }
}
