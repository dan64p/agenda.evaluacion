﻿using Agenda.Evaluacion.Domain.Entities;
using AgendaEntidad = Agenda.Evaluacion.Domain.Entities.Agenda;
using Agenda.Evaluacion.Domain.Entities.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Agenda.Evaluacion.Persistence.Configurations;

namespace Agenda.Evaluacion.Persistence
{
    public sealed class ApplicationDbContext : 
        IdentityDbContext<Usuario>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<AgendaEntidad> Agendas { get; set; }
        public DbSet<Evento> Eventos { get; set; }
        public DbSet<AgendaEvento> AgendasEventos { get; set; }
        public DbSet<InvitacionEvento> InvitacionesEventos { get; set; }
        public DbSet<TipoEvento> TiposEventos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);

            SeedDataConfiguration.SeedData(modelBuilder);
        }
            
    }
}
