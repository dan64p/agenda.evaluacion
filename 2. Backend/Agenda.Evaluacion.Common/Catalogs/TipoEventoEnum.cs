﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Common.Catalogs
{
    public enum TipoEventoEnum
    {
        Exclusivo = 1,
        Compartido = 2
    }
}
