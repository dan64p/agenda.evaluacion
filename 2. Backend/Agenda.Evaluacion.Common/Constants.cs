﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Common
{
    public static class Constants
    {
        public static readonly string ConnectionString_Agenda = "AGENDA";
        public static readonly string DateFormat_Latino = "dd/MM/yyyy";
        public static readonly string DateTimeFormat_Latino = "dd/MM/yyyy hh:mm:ss";
        public static readonly string TimeFormat_Latino = "hh':'mm";

    }
}
