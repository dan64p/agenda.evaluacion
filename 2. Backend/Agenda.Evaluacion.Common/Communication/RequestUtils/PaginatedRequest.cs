﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Evaluacion.Common.Communication.RequestUtils
{
    public class PaginatedRequest<T>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; } = string.Empty;
        public string OrderDir { get; set; } = string.Empty;
        public T Filter { get; set; }
    }
}
