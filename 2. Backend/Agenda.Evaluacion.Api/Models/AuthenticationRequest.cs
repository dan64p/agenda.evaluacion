﻿namespace Agenda.Evaluacion.Api.Models;
public record AuthenticationRequest(string Email, string Password);
