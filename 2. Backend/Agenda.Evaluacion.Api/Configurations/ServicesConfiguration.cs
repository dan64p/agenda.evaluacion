﻿using Agenda.Evaluacion.Agenda.Evaluacion.Application.Abstractions.Validation;
using Agenda.Evaluacion.Api.Middlewares;
using Agenda.Evaluacion.Application.Configurations.Adapters;
using Agenda.Evaluacion.Domain.Repositories;
using Agenda.Evaluacion.Persistence.Repositories;
using FluentValidation;
using MediatR;

namespace Agenda.Evaluacion.Api.Configurations
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services) {

            services.AddScoped<IAgendaEventoRepository, AgendaEventoRepository>();
            services.AddScoped<IEventoRepository, EventoRepository>();
            services.AddScoped<IInvitacionEventoRepository, InvitacionEventoRepository>();
            services.AddScoped<ITipoEventoRepository, TipoEventoRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }

        public static IServiceCollection AddCustomMiddlewares(this IServiceCollection services)
        {
            services.AddTransient<ExceptionHandlingMiddleware>();

            return services;
        }

        public static IServiceCollection AddHelpers(this IServiceCollection services)
        {
            var applicationAssembly = typeof(Application.AssemblyReference).Assembly;

            services.AddMediatR((config) => { 
                config.RegisterServicesFromAssembly(applicationAssembly);
            });

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));

            services.AddValidatorsFromAssembly(applicationAssembly);

            AdaptersConfiguration.Configure();

            return services;
        }
    }
}
