﻿using Agenda.Evaluacion.Application.Evento.Queries.ListarTipoEvento;
using Agenda.Evaluacion.Application.Models.Evento;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Agenda.Evaluacion.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TiposEventosController : ControllerBase
    {
        private readonly ISender _sender;

        public TiposEventosController(ISender sender)
        {
            _sender = sender;
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<TipoEventoSelectViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> ListarTiposEventos(CancellationToken cancellationToken)
        {
            var tiposEventos = await _sender.Send(new ListarTipoEventoQuery(), cancellationToken);

            return Ok(tiposEventos);
        }
    }
}
