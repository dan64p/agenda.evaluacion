﻿using Agenda.Evaluacion.Application.Evento.Commands.ActualizarEvento;
using Agenda.Evaluacion.Application.Evento.Commands.CrearEvento;
using Agenda.Evaluacion.Application.Evento.Commands.EliminarEvento;
using Agenda.Evaluacion.Application.Evento.Queries.BuscarEventoPorId;
using Agenda.Evaluacion.Application.Evento.Queries.BuscarEventosCalendario;
using Agenda.Evaluacion.Application.Evento.Queries.BuscarEventosPaginado;
using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Application.Models.Evento.Filters;
using Agenda.Evaluacion.Common.Communication.RequestUtils;
using Agenda.Evaluacion.Common.Communication.ResponseUtils;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Agenda.Evaluacion.Api.Controllers
{
    [Route("api/v1/eventos/calendario")]
    [ApiController]
    public class EventosCalendarioController : ControllerBase
    {
        private readonly ISender _sender;

        public EventosCalendarioController(ISender sender)
        {
            _sender = sender;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<EventoCalendarioViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> BuscarEventosCalendario(
            [FromQuery] EventoCalendarioFiltro request, 
            CancellationToken cancellationToken
        ) 
        {
            var eventos = await _sender.Send(new BuscarEventosCalendarioQuery(request), cancellationToken);

            return Ok(eventos);
        }
    }
}
