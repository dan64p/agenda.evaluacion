﻿using Agenda.Evaluacion.Application.Evento.Commands.ActualizarEvento;
using Agenda.Evaluacion.Application.Evento.Commands.CrearEvento;
using Agenda.Evaluacion.Application.Evento.Commands.EliminarEvento;
using Agenda.Evaluacion.Application.Evento.Queries.BuscarEventoPorId;
using Agenda.Evaluacion.Application.Evento.Queries.BuscarEventosPaginado;
using Agenda.Evaluacion.Application.Models.Evento;
using Agenda.Evaluacion.Application.Models.Evento.Filters;
using Agenda.Evaluacion.Common.Communication.RequestUtils;
using Agenda.Evaluacion.Common.Communication.ResponseUtils;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Agenda.Evaluacion.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EventosController : ControllerBase
    {
        private readonly ISender _sender;

        public EventosController(ISender sender)
        {
            _sender = sender;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedResponse<EventoGridViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> BuscarEventosPaginadoPorFiltro(
            [FromQuery] PaginatedRequest<EventoGridFiltro> request, 
            CancellationToken cancellationToken
        ) 
        {
            var paginatedResponse = await _sender.Send(new BuscarEventosPaginadoQuery(request), cancellationToken);

            return Ok(paginatedResponse);
        }

        [HttpGet("{idEvento:int}")]
        [ProducesResponseType(typeof(EventoViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> BuscarEventoPorId(int idEvento, CancellationToken cancellationToken)
        {
            var evento = await _sender.Send(new BuscarEventoPorIdQuery(idEvento), cancellationToken);

            return Ok(evento);
        }

        [HttpPost]
        [ProducesResponseType(typeof(CrearEventoResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CrearEvento(
            [FromBody] CrearEventoRequest request, 
            CancellationToken cancellationToken
        )
        {
            var command = request.Adapt<CrearEventoCommand>();

            var evento = await _sender.Send(command, cancellationToken);

            return CreatedAtAction(nameof(CrearEvento), new { IdEvento = evento.IdEvento }, evento);
        }

        [HttpPut("{idEvento:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ActualizarEvento(
            int idEvento, 
            [FromBody] ActualizarEventoRequest request, 
            CancellationToken cancellationToken
        )
        {
            var command = request.Adapt<ActualizarEventoCommand>() with
            {
                IdEvento = idEvento
            };

            await _sender.Send(command, cancellationToken);

            return NoContent();
        }

        [HttpDelete("{idEvento:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> EliminarEvento(
            int idEvento,
            [FromBody] EliminarEventoRequest request,
            CancellationToken cancellationToken
        )
        {
            var command = request.Adapt<EliminarEventoCommand>() with
            {
                IdEvento = idEvento
            };

            await _sender.Send(command, cancellationToken);

            return NoContent();
        }
    }
}
