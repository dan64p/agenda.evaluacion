import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { IPaginatedRequest } from 'src/app/shared/models/request';
import { IPaginatedResponse } from 'src/app/shared/models/response';
import { NotFoundError } from 'src/app/shared/errors/not-found.error';
import { BadRequestError } from 'src/app/shared/errors/bad-request.error';
import { AppError } from 'src/app/shared/errors/app.error';

export class DataService 
{
    constructor(
        private url: string, 
        private http: HttpClient
    ) {}

    get(id: number) {
        return this.handleResponse(this.http.get(this.url + "/" + id));                    
    }

    getAll(request: any) {
        
        const params = this.getQueryParamsFromFilter(request);

        return this.handleResponse( 
            this.http.get<any[]>(this.url, {
                params: params
            })
        );
    }

    getAllPaginated(request: IPaginatedRequest<any>) {
        let params = this.getQueryParamsFromFilter(request.filter, 'filter');
        params = params.append("page", request.page + "");
        params = params.append("pageSize", request.pageSize + "");
        params = params.append("orderBy", request.orderBy + "");
        params = params.append("orderDir", request.orderDir + "");

        return this.handleResponse( 
            this.http.get<IPaginatedResponse<any>>(this.url, {
                params: params
            })
        );
    }

    create(request: any) {
        return this.handleResponse(this.http.post(this.url, request));
    }

    update(
        id: number,
        request: any
    ) {
        return this.handleResponse(this.http.put(this.url + "/" + id, request));
    }

    delete(
        idEvento: number,
        request: any
    ) {
        return this.handleResponse(this.http.delete(this.url + "/" + idEvento, {
            body: request
        }));
    }

    private handleResponse(response: Observable<any>) {
        response = response.pipe(
            catchError((error) => {
                throw this.getError(error);
            })
        );
        return response;
    }

    private getError(error: Response) {
        if (error.status == 404)
            return new NotFoundError(error);
        else if (error.status == 400 || error.status == 422)
            return new BadRequestError(error);
        else
            return new AppError(error);
    }

    private getQueryParamsFromFilter(filter: any, filterPrefix: string = null) {
        let params: HttpParams = new HttpParams();

        for(let prop in filter)
        {
            const paramName = filterPrefix ? filterPrefix + '.' + prop : prop;
            params = params.append(paramName, filter[prop] + '');
        }

        return params;
    }
}
