import * as moment from "moment";

export function obtenerFechaYHoraFormatoEstandar(fechaSinFormato: Date | string, incluirHora: boolean = true) {
        if (!fechaSinFormato) return '';
        
        return incluirHora ? 
                moment(fechaSinFormato).format('DD/MM/yyyy HH:mm:ss') : 
                moment(fechaSinFormato).format('DD/MM/yyyy');
}

export function obtenerFechaFormatoParaServidor(fechaSinFormato: Date | string) {
        return moment(fechaSinFormato).format('yyyy-MM-DD');
}

export function obtenerHoraFormatoParaServidor(fechaSinFormato: Date | string) {
        return moment(fechaSinFormato).format('HH:mm') + ":00";
}