export function descargarArchivoBlob(
    data: any, 
    type: string, 
    fileName: string
)
{
    let blob = new Blob([data], { type: type });
    let url = window.URL.createObjectURL(blob);

    var downloadLink = document.createElement("a");
    downloadLink.href = url;
    downloadLink.download = fileName;

    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}