export interface IPaginatedResponse<T> {
    page: number;
    pageSize: number;
    totalRecords: number;
    data: T[];
}