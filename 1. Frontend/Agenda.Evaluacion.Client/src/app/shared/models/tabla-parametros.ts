export enum TablaTipoParametroEnum {
    ORIGENTRAMITE = 1
}

export interface IParametroFiltro {
    idtipoparametro: number,
    txparametro: string,
    txvalor: string,
    txdescripcion: string
}

export interface IParametroNavegation {
    idtipoparametro: number,
    txparametro: string,
    txvalor: string,
    txdescripcion: string
}

export interface IEstadoNavigation {
    id: number,
    txestado: string,
    txtipoestado: string
}