export interface IPaginatedRequest<T> {
    page: number;
    pageSize: number;
    orderBy: string;
    orderDir: string;
    filter: T;
}

