export class AppError {
    errorMessageResume: string;
    constructor(public originalError) {
        let errorMessage: string = '';
        const { error: appError } = originalError;
        
        for(let prop in appError.errors)
            appError.errors[prop].forEach(error => {
                errorMessage += '- ' + error;    
            });

        this.errorMessageResume = errorMessage;
    }
}