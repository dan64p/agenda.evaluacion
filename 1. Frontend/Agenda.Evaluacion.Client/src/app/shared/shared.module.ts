import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule } from 'primeng/treetable';
import { ChartModule } from 'primeng/chart';
import { MenuModule } from 'primeng/menu';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { StyleClassModule } from 'primeng/styleclass';
import { PanelMenuModule } from 'primeng/panelmenu';
import { TabViewModule } from 'primeng/tabview';
import { InputTextModule } from 'primeng/inputtext';
import { DialogModule } from 'primeng/dialog';
import { TabMenuModule } from 'primeng/tabmenu';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { FileUploadModule } from 'primeng/fileupload';
import { ToastModule } from 'primeng/toast';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DialogService } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { SpeedDialModule } from 'primeng/speeddial';
import { ContextMenuModule } from 'primeng/contextmenu';
import { ToolbarModule } from 'primeng/toolbar';
import { SplitButtonModule } from 'primeng/splitbutton';
import { DividerModule } from 'primeng/divider';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { SISEEstandarDatePipe } from './pipes/sise-date.pipe';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { SISEPesoArchivoMBPipe } from './pipes/sise-pesoarchivo-mb.pipe';
import { ChipsModule } from 'primeng/chips';
import { MessagesModule } from 'primeng/messages';
import { FullCalendarModule } from '@fullcalendar/angular';

@NgModule({
    declarations: [
        SISEEstandarDatePipe,
        SISEPesoArchivoMBPipe
    ],
    imports: [
        FormsModule,
        TreeModule,
        TreeTableModule,
        ChartModule,
        MenuModule,
        TableModule,
        StyleClassModule,
        PanelMenuModule,
        ButtonModule,
        TabViewModule,
        InputTextModule,
        DialogModule,
        TabMenuModule,
        ReactiveFormsModule,
        DropdownModule,
        CalendarModule,
        FileUploadModule,
        ToastModule,
        InputTextareaModule,
        SpeedDialModule,
        ContextMenuModule,
        ToolbarModule,
        SplitButtonModule,
        DividerModule,
        ConfirmDialogModule,
        ToggleButtonModule,
        ChipsModule,
        MessagesModule,
        FullCalendarModule
    ],
    exports: [
        FormsModule,
        TreeModule,
        TreeTableModule,
        ChartModule,
        MenuModule,
        TableModule,
        StyleClassModule,
        PanelMenuModule,
        ButtonModule,
        TabViewModule,
        InputTextModule,
        DialogModule,
        TabMenuModule,
        ReactiveFormsModule,
        DropdownModule,
        CalendarModule,
        FileUploadModule,
        ToastModule,
        InputTextareaModule,
        SpeedDialModule,
        ContextMenuModule,
        ToolbarModule,
        SplitButtonModule,
        DividerModule,
        ConfirmDialogModule,
        SISEEstandarDatePipe,
        SISEPesoArchivoMBPipe,
        ToggleButtonModule,
        ChipsModule,
        MessagesModule,
        FullCalendarModule
    ],
    providers: [DialogService, MessageService, ConfirmationService],
})
export class SharedModule {}
