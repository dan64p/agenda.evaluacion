import { Pipe, PipeTransform } from '@angular/core';
import { obtenerFechaYHoraFormatoEstandar } from '../functions/dates';

@Pipe({ name: 'siseestandardate' })
export class SISEEstandarDatePipe implements PipeTransform {
  transform(date: Date | string, incluirHora: boolean): string {
    return obtenerFechaYHoraFormatoEstandar(date, incluirHora);
  }
}