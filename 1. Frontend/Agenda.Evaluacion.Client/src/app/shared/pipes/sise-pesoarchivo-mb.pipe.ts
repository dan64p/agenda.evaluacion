import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'sisepesoarchivomb' })
export class SISEPesoArchivoMBPipe implements PipeTransform {
  transform(pesoBytes: any): string {
    if (!pesoBytes) return '';
    let pesoBytesNumerico: any = pesoBytes instanceof Number ? pesoBytes : parseInt(pesoBytes);
    return (Math.round((pesoBytesNumerico / 1024 / 1024) * 100) / 100).toFixed(2) + ' MB';
  }
}