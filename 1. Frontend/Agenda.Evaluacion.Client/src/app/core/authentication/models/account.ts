export interface IUser {
    idUsuario: string;
    nombreCompleto: string;
    email: string;
    roles: IUserRol[];
    idAgendaPrincipal: number;
}

export interface IUserRol {
    idRol: string;
    nombreRol: string;
}

export class TestUser implements IUser {
    idUsuario = "1ec0e447-6149-4889-a039-2420a1140b52";
    nombreCompleto = "Juan Perez";
    email = "juan.perez@test.com";
    roles = [
        { idRol: '5a0a563d-936b-4152-af1b-27f8526e10d7', nombreRol: 'UsuarioEstandar' }
    ];
    idAgendaPrincipal = 1;
}