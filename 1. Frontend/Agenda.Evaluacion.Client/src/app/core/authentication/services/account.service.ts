import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { IUser, TestUser } from '../models/account';

@Injectable({ providedIn: 'root' })
export class AccountService {
    private userSubject: BehaviorSubject<IUser | null>;
    public user: Observable<IUser | null>;

    constructor(private http: HttpClient) {
        this.userSubject = new BehaviorSubject(new TestUser());
        this.user = this.userSubject.asObservable();
    }

    public get userValue() {
        return this.userSubject.value;
    }

    login(username: string, password: string) {
       
    }

    logout() {
       
    }
}
