import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { AccountService } from './services/account.service';

@NgModule({
    imports: [
        CommonModule,
        AuthRoutingModule
    ],
    providers: []
})
export class AuthModule { }
