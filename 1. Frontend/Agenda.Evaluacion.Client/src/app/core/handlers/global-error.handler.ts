import { ErrorHandler, Injectable, Injector } from "@angular/core";
import { MessageService } from "primeng/api";

export class GlobalErrorHandler implements ErrorHandler {
    constructor(private messageService: MessageService) {
    }

    handleError(error: any): void {
        this.messageService.add({
            key: 'agendatoast',
            severity: 'error',
            summary: 'Error',
            detail: 'Ocurrió un error general en el sistema. Recargue la página y si el problema continua comuníquese con el Administrador del Sistema.'
        });
    }
}