import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppLayoutModule } from './layout/app.layout.module';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { AuthModule } from './authentication/auth.module';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [NotfoundComponent],
    imports: [AppLayoutModule, AuthModule, RouterModule],
    exports: [NotfoundComponent],
})
export class CoreModule {}
