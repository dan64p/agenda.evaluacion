import { ErrorHandler, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { GlobalErrorHandler } from './core/handlers/global-error.handler';
import { MessageModule } from 'primeng/message';
import { MessageService } from 'primeng/api';
import { SharedModule } from './shared/shared.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        AppRoutingModule, 
        CoreModule,
        SharedModule
    ],
    providers: [
        MessageService,
        { 
            provide: ErrorHandler, 
            useClass: GlobalErrorHandler,
            deps: [MessageService]
        }
    ],
    bootstrap: [
        AppComponent
    ],
})
export class AppModule {}
