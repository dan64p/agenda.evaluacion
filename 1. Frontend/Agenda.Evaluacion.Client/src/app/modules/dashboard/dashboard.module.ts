import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DashboardsRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardService } from './service/dashboard.service';

@NgModule({
    imports: [CommonModule, FormsModule, SharedModule, DashboardsRoutingModule],
    declarations: [DashboardComponent],
    providers: [DashboardService],
})
export class DashboardModule {}
