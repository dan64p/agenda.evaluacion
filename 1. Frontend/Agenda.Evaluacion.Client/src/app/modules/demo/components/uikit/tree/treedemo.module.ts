import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TreeDemoRoutingModule } from './treedemo-routing.module';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule } from 'primeng/treetable';
import { TreeDemoComponent } from './treedemo.component';

@NgModule({
    declarations: [TreeDemoComponent],
    imports: [
        CommonModule,
        TreeDemoRoutingModule,
        FormsModule,
        TreeModule,
        TreeTableModule,
    ],
})
export class TreeDemoModule {}
