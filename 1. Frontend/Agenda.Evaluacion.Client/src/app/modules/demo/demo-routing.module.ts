import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                loadChildren: () =>
                    import('./components/dashboard/dashboard.module').then(
                        (m) => m.DashboardModule
                    ),
            },
            {
                path: 'uikit',
                loadChildren: () =>
                    import('./components/uikit/uikit.module').then(
                        (m) => m.UIkitModule
                    ),
            },
            {
                path: 'utilities',
                loadChildren: () =>
                    import('./components/utilities/utilities.module').then(
                        (m) => m.UtilitiesModule
                    ),
            },
            {
                path: 'documentation',
                loadChildren: () =>
                    import(
                        './components/documentation/documentation.module'
                    ).then((m) => m.DocumentationModule),
            },
            {
                path: 'blocks',
                loadChildren: () =>
                    import('./components/primeblocks/primeblocks.module').then(
                        (m) => m.PrimeBlocksModule
                    ),
            },
            {
                path: 'pages',
                loadChildren: () =>
                    import('./components/pages/pages.module').then(
                        (m) => m.PagesModule
                    ),
            },
        ]),
    ],
    exports: [RouterModule],
})
export class DemoModuleRoutingModule {}
