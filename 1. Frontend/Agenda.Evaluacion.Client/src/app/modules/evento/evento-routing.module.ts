import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EventoContainerComponent } from './container/evento-container.component';
import { EdicionEventoComponent } from './views/edicion-evento/edicion-evento.component';
import { CalendarioEventoComponent } from './views/calendario-evento/calendario-evento.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { 
                path: '', 
                component: EventoContainerComponent,
                children: [
                    {
                        path: 'edicion',
                        component: EdicionEventoComponent
                    },
                    {
                        path: '',
                        component: CalendarioEventoComponent
                    }                      
                ]
            },
        ]),
    ],
    exports: [RouterModule],
})
export class EventoRoutingModule {}
