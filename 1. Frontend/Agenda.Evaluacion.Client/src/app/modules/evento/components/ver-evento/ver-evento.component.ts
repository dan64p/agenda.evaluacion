import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AccountService } from 'src/app/core/authentication/services/account.service';
import { EventoService } from 'src/app/modules/evento/service/evento.service';
import { obtenerEditarEventoRequest } from 'src/app/modules/evento/utiles/evento-utiles';
import { obtenerFechaFormatoParaServidor, obtenerHoraFormatoParaServidor } from 'src/app/shared/functions/dates';
import { IParametroFiltro } from 'src/app/shared/models/tabla-parametros';

@Component({
    templateUrl: './ver-evento.component.html',
})
export class VerEventoComponent implements OnInit {
    private idEvento: number;
    form: FormGroup;

    constructor(
        public ref: DynamicDialogRef,
        public config: DynamicDialogConfig,
        private fb: FormBuilder,
        private eventoService: EventoService,
        private messageService: MessageService,
        private accountService: AccountService
    ) {
    }

    ngOnInit() {
        this.inicializarComponente();
    }

    ngAfterViewInit() {
        this.eventoService.get(this.idEvento)
            .subscribe((eventoSinFormato) => {
                const evento: any = { ...eventoSinFormato };

                evento.fechaInicio = new Date(evento.fechaInicio);
                evento.horaInicio = new Date(evento.horaInicio);
                evento.fechaFin = new Date(evento.fechaFin);
                evento.horaFin = new Date(evento.horaFin);
                evento.nombresParticipantes = evento.participantes.map(participante => participante.nombreCompleto);

                delete evento.participantes;

                this.form.patchValue(evento);
            });
    }


    handleCerrarModal() {
        this.ref.close(false);
    }

    private inicializarComponente() {
        this.construirFormulario();
    }

    private construirFormulario() {
        this.idEvento = this.config.data.idEvento;
        this.form = this.fb.group({});
    }
}
