import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AccountService } from 'src/app/core/authentication/services/account.service';
import { ITipoEventoFiltroViewModel } from 'src/app/modules/evento/models/tipo-evento';
import { TipoEventoService } from 'src/app/modules/evento/service/tipo-evento.service';

@Component({
    selector: 'app-editar-evento',
    templateUrl: './editar-evento.component.html',
})
export class EditarEventoComponent implements OnInit {
    @Input() form: FormGroup;
    @Input() soloLectura: boolean = false;
    tiposEventos: ITipoEventoFiltroViewModel[] = [];

    constructor(
        private fb: FormBuilder,
        
        private tipoEventoService: TipoEventoService,
    ) {
    }

    get Nombre() {
        return this.form.get('nombre');
    }

    get Descripcion() {
        return this.form.get('descripcion');
    }

    get FechaInicio() {
        return this.form.get('fechaInicio');
    }

    get HoraInicio() {
        return this.form.get('horaInicio');
    }

    get FechaFin() {
        return this.form.get('fechaFin');
    }

    get HoraFin() {
        return this.form.get('horaFin');
    }

    get IdTipoEvento() {
        return this.form.get('idTipoEvento');
    }

    ngOnInit() {
        this.inicializarComponente();
    }

    private inicializarComponente() {
        this.cargarDependencias();
        this.construirFormulario();
    }

    private cargarDependencias() {
        this.tipoEventoService.listaTiposEventosFiltro()
            .subscribe((tiposEventos) => this.tiposEventos = tiposEventos);
    }

    private construirFormulario() {
        this.form.addControl('nombre', new FormControl('', [Validators.required, Validators.maxLength(300)]));
        this.form.addControl('descripcion', new FormControl('', [Validators.required, Validators.maxLength(500)]));
        this.form.addControl('fechaInicio', new FormControl('', [Validators.required]));
        this.form.addControl('horaInicio', new FormControl('', [Validators.required]));
        this.form.addControl('fechaFin', new FormControl('', [Validators.required]));
        this.form.addControl('horaFin', new FormControl('', [Validators.required]));
        this.form.addControl('local', new FormControl(''));
        this.form.addControl('idTipoEvento', new FormControl(null, [Validators.required]));
        this.form.addControl('nombresParticipantes', new FormControl<string[] | null>(null)); 

        if (this.soloLectura)
            this.form.disable();

    }
}
