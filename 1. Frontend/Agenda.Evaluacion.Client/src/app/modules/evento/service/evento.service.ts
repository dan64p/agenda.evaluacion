import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Observable, of, throwError } from 'rxjs';
import { IActualizarEventoRequest, IActualizarEventoResponse, ICrearEventoRequest, ICrearEventoResponse, IEliminarEventoRequest, IEliminarEventoResponse, IEventoEdicionViewModel, IEventoGridFiltro, IEventoGridViewModel } from '../models/evento';
import { IPaginatedRequest } from 'src/app/shared/models/request';
import { IPaginatedResponse } from 'src/app/shared/models/response';
import { NotFoundError } from 'src/app/shared/errors/not-found.error';
import { BadRequestError } from 'src/app/shared/errors/bad-request.error';
import { AppError } from 'src/app/shared/errors/app.error';
import { DataService } from 'src/app/shared/services/data.service';

@Injectable()
export class EventoService extends DataService{
    constructor(http: HttpClient) {
        super(
            environment.urlApi + environment.urlApiVersion + '/Eventos', 
            http
        );
    }
}
