import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { of } from 'rxjs';
import { descargarArchivoBlob } from 'src/app/shared/functions/files';
import { ITipoEventoFiltroViewModel } from '../models/tipo-evento';

@Injectable()
export class TipoEventoService {
    private urlExpediente: string = environment.urlApi + environment.urlApiVersion + '/TiposEventos';

    constructor(private http: HttpClient) {}

    listaTiposEventosFiltro() {
        return this.http
            .get<ITipoEventoFiltroViewModel[]>(this.urlExpediente);            
    }
}
