import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Observable, of, throwError } from 'rxjs';
import { IActualizarEventoRequest, IActualizarEventoResponse, ICrearEventoRequest, ICrearEventoResponse, IEliminarEventoRequest, IEliminarEventoResponse, IEventoEdicionViewModel, IEventoGridFiltro, IEventoGridViewModel } from '../models/evento';
import { IPaginatedRequest } from 'src/app/shared/models/request';
import { IPaginatedResponse } from 'src/app/shared/models/response';
import { NotFoundError } from 'src/app/shared/errors/not-found.error';
import { BadRequestError } from 'src/app/shared/errors/bad-request.error';
import { AppError } from 'src/app/shared/errors/app.error';

@Injectable()
export class EventoService {
    private urlEvento: string = environment.urlApi + environment.urlApiVersion + '/Eventos';

    constructor(private http: HttpClient) {}

    buscarEventoPorId(idEvento: number) {
        return this.handleResponse(this.http.get<IEventoEdicionViewModel>(this.urlEvento + "/" + idEvento));                    
    }

    buscarEventoPaginadoPorFiltro(request: IPaginatedRequest<IEventoGridFiltro>) {
        let params: HttpParams = new HttpParams();
        params = params.append("page", request.page + "");
        params = params.append("pageSize", request.pageSize + "");
        params = params.append("orderBy", request.orderBy + "");
        params = params.append("orderDir", request.orderDir + "");
        params = params.append("filter.idUsuario", request.filter.idUsuario);
        params = params.append("filter.fechaEvento", request.filter.fechaEvento + "");
        params = params.append("filter.horaEvento", request.filter.horaEvento + "");
        params = params.append("filter.filtroEvento", request.filter.filtroEvento + "");

        return this.handleResponse( 
            this.http.get<IPaginatedResponse<IEventoGridViewModel>>(this.urlEvento, {
                params: params
            })
        );
    }

    crearEvento(request: ICrearEventoRequest) {
        return this.handleResponse(this.http.post<ICrearEventoResponse>(this.urlEvento, request));
    }

    actualizarEvento(
        idEvento: number,
        request: IActualizarEventoRequest
    ) {
        return this.handleResponse(this.http.put<IActualizarEventoResponse>(this.urlEvento + "/" + idEvento, request));
    }

    eliminarEvento(
        idEvento: number,
        request: IEliminarEventoRequest
    ) {
        return this.handleResponse(this.http.delete<IEliminarEventoResponse>(this.urlEvento + "/" + idEvento, {
            body: request
        }));
    }

    private handleResponse(response: Observable<any>) {
        response.pipe(
            catchError((error) => {
                return throwError(() => {
                    return this.getError(error);
                })
            })
        );
        return response;
    }

    private getError(error: Response) {
        if (error.status == 404)
            return new NotFoundError(error);
        else if (error.status == 400)
            return new BadRequestError(error);
        else
            return new AppError(error);
    }
}
