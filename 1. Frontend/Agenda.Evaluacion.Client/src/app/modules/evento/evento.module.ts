import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { EventoService } from './service/evento.service';
import { EventoRoutingModule } from './evento-routing.module';
import { EventoContainerComponent } from './container/evento-container.component';
import { EdicionEventoComponent } from './views/edicion-evento/edicion-evento.component';
import { CrearEventoComponent } from './views/edicion-evento/components/creacion-evento/crear-evento.component';
import { ActualizarEventoComponent } from './views/edicion-evento/components/actualizar-evento/actualizar-evento.component';
import { TipoEventoService } from './service/tipo-evento.service';
import { EditarEventoComponent } from './components/editar-evento/editar-evento.component';
import { VerEventoComponent } from './components/ver-evento/ver-evento.component';
import { CalendarioEventoComponent } from './views/calendario-evento/calendario-evento.component';
import { CalendarioService } from './service/calendario.service';

@NgModule({
    declarations: [
        EventoContainerComponent,
        EdicionEventoComponent,
        CrearEventoComponent,
        ActualizarEventoComponent,
        EditarEventoComponent,
        VerEventoComponent,
        CalendarioEventoComponent
    ],
    imports: [
        CommonModule, 
        SharedModule,
        EventoRoutingModule
    ],
    providers: [EventoService, TipoEventoService, CalendarioService],
})
export class EventoModule {}
