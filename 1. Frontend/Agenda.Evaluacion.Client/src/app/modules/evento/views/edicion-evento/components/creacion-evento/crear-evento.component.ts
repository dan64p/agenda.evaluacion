import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AccountService } from 'src/app/core/authentication/services/account.service';
import { ITipoEventoFiltroViewModel } from 'src/app/modules/evento/models/tipo-evento';
import { EventoService } from 'src/app/modules/evento/service/evento.service';
import { TipoEventoService } from 'src/app/modules/evento/service/tipo-evento.service';
import { obtenerEditarEventoRequest } from 'src/app/modules/evento/utiles/evento-utiles';
import { BadRequestError } from 'src/app/shared/errors/bad-request.error';
import { obtenerFechaFormatoParaServidor, obtenerHoraFormatoParaServidor } from 'src/app/shared/functions/dates';

@Component({
    templateUrl: './crear-evento.component.html',
})
export class CrearEventoComponent implements OnInit {
    form: FormGroup;

    constructor(
        public ref: DynamicDialogRef,
        public config: DynamicDialogConfig,
        private fb: FormBuilder,
        private eventoService: EventoService,
        private messageService: MessageService,
        private accountService: AccountService
    ) {
    }

    ngOnInit() {
        this.inicializarComponente();
    }

    handleCrearEvento() {
        const request = obtenerEditarEventoRequest({ ...this.form.value });
        
        this.eventoService.create(request)
            .subscribe(
                {
                    next: (response) => {
                    
                        this.messageService.add({
                            severity: 'success',
                            summary: 'Éxito',
                            detail: 'Se creó el evento correctamente.'
                        });
    
                        this.ref.close(true);
                    },
                    error: (error) => {
                        if (error instanceof BadRequestError)
                        {
                            this.messageService.add({
                                severity: 'warn',
                                summary: 'Advertencia',
                                detail: error.errorMessageResume
                            });
                        }
                        else
                        {
                            throw error;
                        }
                    }
                }           
            );
    }

    handleCerrarModal() {
        this.ref.close(false);
    }   

    private inicializarComponente() {
        this.construirFormulario();
    }

    private construirFormulario() {
        const { idUsuario, idAgendaPrincipal } = this.accountService.userValue;

        this.form = this.fb.group({});
        this.form.addControl('idUsuarioCreador', new FormControl(idUsuario, [Validators.required]));
        this.form.addControl('idAgenda', new FormControl(idAgendaPrincipal, [Validators.required]));
    }    
}
