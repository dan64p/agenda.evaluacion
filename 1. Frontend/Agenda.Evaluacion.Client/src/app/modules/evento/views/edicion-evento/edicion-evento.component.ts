import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { EventoService } from '../../service/evento.service';
import { EventoGridFiltroPaginado, IEventoGridFiltro, IEventoGridViewModel } from '../../models/evento';
import { IPaginatedRequest } from 'src/app/shared/models/request';
import { AccountService } from 'src/app/core/authentication/services/account.service';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { CrearEventoComponent } from './components/creacion-evento/crear-evento.component';
import { ActualizarEventoComponent } from './components/actualizar-evento/actualizar-evento.component';
import { obtenerFechaFormatoParaServidor, obtenerHoraFormatoParaServidor } from 'src/app/shared/functions/dates';
import { VerEventoComponent } from '../../components/ver-evento/ver-evento.component';
import { NotFoundError } from 'src/app/shared/errors/not-found.error';
import { BadRequestError } from 'src/app/shared/errors/bad-request.error';

@Component({
    selector: 'app-edicion-evento',
    templateUrl: './edicion-evento.component.html',
    styleUrls: ['edicion-evento.component.scss']
})
export class EdicionEventoComponent implements OnInit {
    eventos: IEventoGridViewModel[] = [];
    totalRegistro: number = 0;
    cargando: boolean = false;
    mostrarFiltro: boolean = false;
    primeraCarga: boolean = true;
    request: IPaginatedRequest<IEventoGridFiltro> = new EventoGridFiltroPaginado(this.accountService.userValue.idUsuario);
    filtro: IEventoGridFiltro = {
        idUsuario: this.accountService.userValue.idUsuario
    };
    eventoSeleccionadoOpciones: IEventoGridViewModel;
    refEditarDocumento: DynamicDialogRef;
    opcionesEventoSeleccionado: any[] = [
        {
            label: 'Vista Previa',
            icon: 'pi pi-eye',
            command: () => {
            }
        },
    ];
    
    constructor(
        private eventoService: EventoService,
        private accountService: AccountService,
        public dialogService: DialogService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
    ) {}

    ngOnInit() {
        this.inicializarComponente();
    }

    handleOnLoad(event) {
        if (this.primeraCarga)
        {
            this.primeraCarga = !this.primeraCarga;
            return;
        }

        const actualPage = event.first / event.rows + 1;
        this.request.page = actualPage;
        this.request.pageSize = event.rows;
        this.buscarEventoPaginadoPorFiltro();
    }

    handleLimpiarBusqueda() {
        this.filtro = {
            idUsuario: this.accountService.userValue.idUsuario
        };
        this.handleBuscar();
    }

    handleBuscar() {
        this.request.page = 1;
        this.buscarEventoPaginadoPorFiltro();
    }

    handleCrearEvento() {
        this.refEditarDocumento = this.dialogService.open(
            CrearEventoComponent,
            {
                data: {
                },
                header: 'Crear Evento',
            }
        );

        this.refEditarDocumento.onClose.subscribe((agregado) => {
            if (agregado)
            {
                this.request.page = 1;
                this.buscarEventoPaginadoPorFiltro();
            }
        });
    }

    handleVerEvento(evento) {
        this.refEditarDocumento = this.dialogService.open(
            VerEventoComponent,
            {
                data: {
                    idEvento: evento.idEvento
                },
                header: 'Ver Evento',
            }
        );

        this.refEditarDocumento.onClose.subscribe((agregado) => {
            
        });
    }

    handleActualizarEvento(evento) {
        this.refEditarDocumento = this.dialogService.open(
            ActualizarEventoComponent,
            {
                data: {
                    idEvento: evento.idEvento
                },
                header: 'Actualizar Evento',
            }
        );

        this.refEditarDocumento.onClose.subscribe((agregado) => {
            if (agregado)
            {
                this.request.page = 1;
                this.buscarEventoPaginadoPorFiltro();
            }
        });
    }

    handleEliminarEvento(evento) 
    {
        this.confirmationService.confirm({
            message: '¿Está seguro que desea eliminar el evento '+ evento.nombreEvento +'?',
            header: 'Eliminar documento',
            icon: 'pi pi-info-circle',
            acceptLabel: 'Si',
            rejectLabel: 'No',
            accept: () => {
                this.eventoService.delete(evento.idEvento, {
                    idEvento: 0,
                    IdUsuarioElimina: this.accountService.userValue.idUsuario
                })
                    .subscribe({
                        next: (response) => {

                            this.messageService.add({
                                severity: 'success',
                                summary: 'Éxito',
                                detail: 'Se eliminó correctamente el evento.'
                            });
    
                            this.request.page = 1;
                            this.buscarEventoPaginadoPorFiltro();
                        },
                        error: (error) => {
                            if (error instanceof NotFoundError)
                            {
                                this.messageService.add({
                                    severity: 'warn',
                                    summary: 'Advertencia',
                                    detail: 'El evento ya fue eliminado.'
                                });
                            }
                            else if (error instanceof BadRequestError)
                            {
                                this.messageService.add({
                                    severity: 'warn',
                                    summary: 'Advertencia',
                                    detail: error.errorMessageResume
                                });
                            }
                            else
                            {
                                throw error;
                            }
                        }
                    });
            },
            reject: (type) => {
               
            }
        });
    }

    private inicializarComponente() {
        this.buscarEventoPaginadoPorFiltro();
    }

    private formatearFilterRequest() {
        this.request.filter = { ...this.filtro };

        // this.request.filter.fechaEvento = this.filtro.fechaEvento ? 
        //                                     obtenerFechaFormatoParaServidor(this.filtro.fechaEvento) : '';
        // this.request.filter.horaEvento = this.filtro.horaEvento ? 
        //                                     obtenerHoraFormatoParaServidor(this.filtro.horaEvento) : '';
    }

    private buscarEventoPaginadoPorFiltro() {
        this.formatearFilterRequest();
        this.eventos = [];
        this.cargando = true;

        this.eventoService
            .getAllPaginated(this.request)
            .subscribe({ 
                next: (response) => {
                    this.cargando = false;
                    
                    this.eventos = response.data;
                    this.totalRegistro = response.totalRecords;
                }
            });
    }
}
