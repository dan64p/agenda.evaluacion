import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { EventoService } from '../../service/evento.service';
import { EventoGridFiltroPaginado, IEventoGridFiltro, IEventoGridViewModel } from '../../models/evento';
import { IPaginatedRequest } from 'src/app/shared/models/request';
import { AccountService } from 'src/app/core/authentication/services/account.service';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { obtenerFechaFormatoParaServidor, obtenerHoraFormatoParaServidor } from 'src/app/shared/functions/dates';
import { VerEventoComponent } from '../../components/ver-evento/ver-evento.component';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid'
import { CalendarOptions, EventApi } from '@fullcalendar/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import esLocale from '@fullcalendar/core/locales/es';
import { CalendarioService } from '../../service/calendario.service';
import * as moment from 'moment';
import { FullCalendarTypes, IEventoCalendarioFiltro } from '../../models/calendario';

@Component({
    selector: 'app-calendario-evento',
    templateUrl: './calendario-evento.component.html',
    styleUrls: ['calendario-evento.component.scss']
})
export class CalendarioEventoComponent implements OnInit {
    @ViewChild('calendar') calendarComponent: FullCalendarComponent;
    eventos: IEventoGridViewModel[] = [];
    totalRegistro: number = 0;
    cargando: boolean = false;
    mostrarFiltro: boolean = false;
    primeraCarga: boolean = true;
    request: IPaginatedRequest<IEventoGridFiltro> = new EventoGridFiltroPaginado(this.accountService.userValue.idUsuario);
    filtro: IEventoCalendarioFiltro = {
        idUsuario: this.accountService.userValue.idUsuario
    };
    eventoSeleccionadoOpciones: IEventoGridViewModel;
    refEditarDocumento: DynamicDialogRef;
    opcionesEventoSeleccionado: any[] = [
        {
            label: 'Vista Previa',
            icon: 'pi pi-eye',
            command: () => {
            }
        },
    ];
    calendarOptions: CalendarOptions = {
        initialView: 'dayGridMonth',
        locale: esLocale,
        plugins: [dayGridPlugin, timeGridPlugin],
        eventClick: this.handleEventClick.bind(this),
        events: this.handleEvents.bind(this),
        headerToolbar: {
            left: '',
            center: 'title',
            right: 'dayGridMonth,timeGridDay' // user can switch between the two
        },
        eventSourceFailure: (error) => {
            console.log(error);
        }
    };
    private fechaActual: Date = new Date();
    
    constructor(
        private calendarioService: CalendarioService,
        private accountService: AccountService,
        public dialogService: DialogService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
    ) {}

    private get CalendarApi() {
        return this.calendarComponent ? this.calendarComponent.getApi() : null;
    }

    ngOnInit() {
        this.inicializarComponente();
    }

    handleBuscar() {

        this.cambiarFechaCalendario();
    }

    handleLimpiarBuscar() {
        this.filtro = {
            idUsuario: this.accountService.userValue.idUsuario
        };

        this.cambiarFechaCalendario();
    }

    private cambiarFechaCalendario() {
        const { anio, mes, dia } = this.obtenerRequestBuscarCalendarioPorFiltro();
        const mesTexto = '-' + (mes + '').padStart(2, '0');
        const diaTexto = '-' + (dia ?  (dia + '').padStart(2, '0') : '01');
        const fechaBusqueda = anio + mesTexto + diaTexto;

        
        this.CalendarApi.gotoDate(fechaBusqueda);
        this.CalendarApi.refetchEvents();
    }

    handleEvents(info, successCallback, failureCallback) {
        setTimeout(() => {
            const filtro =this.obtenerRequestBuscarCalendarioPorFiltro();
            
            this.calendarioService.getAll(filtro)
                .subscribe({
                    next: (eventosCalendario) => {
                        eventosCalendario = eventosCalendario ? eventosCalendario: [];
                        const eventos = eventosCalendario.map(eventoCal => {
                            return {
                                id: eventoCal.idEvento,
                                title: eventoCal.nombreEvento,
                                start: eventoCal.fechaHoraInicio,
                                end: eventoCal.fechaHoraFin,
                            };
                        });

                        successCallback(eventos);
                    },
                    error: (err) => {
                        failureCallback(err);
                    }
                });
        });
    }

    handleEventClick(info) {
        this.refEditarDocumento = this.dialogService.open(
            VerEventoComponent,
            {
                data: {
                    idEvento: info.event.id
                },
                header: 'Ver Evento',
            }
        );

        this.refEditarDocumento.onClose.subscribe((agregado) => {
            
        });
    }

    private obtenerRequestBuscarCalendarioPorFiltro() {
        const idUsuario = this.accountService.userValue.idUsuario;

        const fechaSeleccionada = this.filtro.fechaEvento ? 
                                    this.filtro.fechaEvento :
                                    this.fechaActual;

        let filtroEvento = this.filtro.filtroEvento ? this.filtro.filtroEvento : '';
        let dia: any = this.filtro.fechaEvento ?  moment(fechaSeleccionada).date() : '';
        let mes: number = moment(fechaSeleccionada).month() + 1;
        let anio: number = moment(fechaSeleccionada).year();
        let hora: string = this.filtro.horaEvento ? 
                                obtenerHoraFormatoParaServidor(this.filtro.horaEvento) :
                                '';

        return {
            idUsuario,
            filtroEvento,
            dia,
            mes,
            anio,
            hora
        };
    }    

    handleVerEvento(evento) {
        this.refEditarDocumento = this.dialogService.open(
            VerEventoComponent,
            {
                data: {
                    idEvento: evento.idEvento
                },
                header: 'Ver Evento',
            }
        );

        this.refEditarDocumento.onClose.subscribe((agregado) => {
            
        });
    }

    private inicializarComponente() {
        //this.filtro.fechaEvento = new Date();
    }
}
