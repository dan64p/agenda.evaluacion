export const FullCalendarTypes = {
    dayGridMonth: 'dayGridMonth',
    timeGridDay: 'timeGridDay'
};

export interface IEventoCalendarioFiltro {
    idUsuario: string;
    fechaEvento?: Date;
    horaEvento?: Date;
    filtroEvento?: string;
}