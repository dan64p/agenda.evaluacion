export interface ITipoEventoFiltroViewModel {
    idTipoEvento: number;
    nombre: string;
    esCompartible: boolean
}