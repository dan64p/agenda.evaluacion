import { IPaginatedRequest } from "src/app/shared/models/request";

export interface IEventoEdicionViewModel {
    idEvento: number;
    nombre: string;
    descripcion: string;
    fechaInicio: string;
    horaInicio: string;
    fechaFin: string;
    horaFin:  string;
    local: string;
    idTipoEvento: number;
    participantes: IParticipanteEventoEdicionViewModel[]
}

export interface IParticipanteEventoEdicionViewModel {
    nombreCompleto: string;
}

export interface IEventoGridFiltro {
    idUsuario: string;
    fechaEvento?: string;
    horaEvento?: string;
    filtroEvento?: string;
}

export interface IEventoGridViewModel {
    idAgendaEvento: number;
    idAgenda: number;
    idEvento: number;
    nombreEvento: string;
    descripcionEvento: string;
    fechaHoraEvento: string;
    idUsuarioCreador: string;
    nombreUsuarioCreador: string;
    fechaRegistro: string;
    idTipoEvento: number;
    nombreTipoEvento: string;
}

export class EventoGridFiltroPaginado implements IPaginatedRequest<IEventoGridFiltro>
{
    constructor(idUsuario: string) {
        this.filter = {
            idUsuario: idUsuario,
            fechaEvento: '',
            horaEvento: '',
            filtroEvento: ''
        };
    }

    page = 1;
    pageSize = 10;
    orderBy = "";
    orderDir = "";
    filter = {
        idUsuario: '',
        fechaEvento: '',
        horaEvento: '',
        filtroEvento: ''
    };
}

export interface ICrearEventoRequest {
    nombre: string;
    descripcion: string;
    fechaInicio: string;
    horaInicio: string;
    fechaFin: string;
    horaFin: string;
    local: string;
    idTipoEvento: number;
    participantes?: IParticipanteCrearEventoRequest[];
    idUsuarioCreador: string;
    idAgenda: number;
}

export interface IParticipanteCrearEventoRequest {
    nombreCompleto: string;
    idUsuario?: number
}

export interface ICrearEventoResponse 
    extends IEdicionEventoResponse
{
}

export interface IActualizarEventoRequest {
    idEvento: number;
    nombre: string;
    descripcion: string;
    fechaInicio: string;
    horaInicio: string;
    fechaFin: string;
    horaFin: string;
    local: string;
    idTipoEvento: number;
    participantes?: IParticipanteActualizarEventoRequest[];
    idUsuarioActualiza: string;
}

export interface IParticipanteActualizarEventoRequest {
    nombreCompleto: string;
    idUsuario?: number
}

export interface IActualizarEventoResponse
    extends IEdicionEventoResponse
{
}

export interface IEliminarEventoRequest {
    idEvento: number;
    IdUsuarioElimina: string;
}

export interface IEliminarEventoResponse 
    extends IEdicionEventoResponse
{

}

export interface IEdicionEventoResponse {
    idEvento: number;
    nombre: string;
}