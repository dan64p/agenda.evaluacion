import { obtenerFechaFormatoParaServidor, obtenerHoraFormatoParaServidor } from "src/app/shared/functions/dates";

export function obtenerEditarEventoRequest(request) {
    request.fechaInicio = obtenerFechaFormatoParaServidor(request.fechaInicio);
    request.horaInicio = obtenerHoraFormatoParaServidor(request.horaInicio);
    request.fechaFin = obtenerFechaFormatoParaServidor(request.fechaFin);
    request.horaFin = obtenerHoraFormatoParaServidor(request.horaFin);

    if (request.nombresParticipantes)
    {
        request.participantes = [...request.nombresParticipantes.map(nombre => {
            return {
                nombreCompleto: nombre,
            };
        })];

        delete request.nombresParticipantes;
    }

    return request;
}